import sys
from osgeo import ogr, osr, gdal
import numpy as np
from scipy import stats
import re
import matplotlib.pyplot as plt
import statistics

gdal.UseExceptions()


################################ Label ##################################################

def autolabel(barobj):
    """Attach a text label above each bar in *rects*, displaying its height."""
    for rect in barobj:
        height = rect.get_height()
        ax2.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom', size='14')



plotsize = [14,8]



##################################### Berlin ####################################



ds = gdal.Open("./../../processing/output/berlin/all_LOD.tif")
# ds = gdal.Open("./../../processing/output/phoenix/all_LOD.tif")
values = np.concatenate(np.array(ds.GetRasterBand(1).ReadAsArray()))
# myarray = np.array([1,2,3,4,5,6])
# print(myarray)
# print(len(myarray))
# print(myarray.max())
# print(myarray.min())


barchart = {
    '2 bis ≤5': 0,
    '>5 bis ≤8': 0,
    '>8 bis ≤11': 0,
    '>11 bis ≤14': 0,
    '>14 bis ≤17': 0,
    '>17 bis ≤20': 0,
    '>20 bis ≤40': 0,
    '>40 bis ≤80': 0,
    '>80 bis ≤160': 0
}

for value in values:
    if value <= 5:
        barchart['2 bis ≤5'] = barchart['2 bis ≤5']+1
    elif value > 5 and value <= 8:
        barchart['>5 bis ≤8'] = barchart['>5 bis ≤8'] + 1
    elif value > 8 and value <= 11:
        barchart['>8 bis ≤11'] = barchart['>8 bis ≤11'] + 1
    elif value > 11 and value <= 14:
        barchart['>11 bis ≤14'] = barchart['>11 bis ≤14'] + 1
    elif value > 14 and value <= 17:
        barchart['>14 bis ≤17'] = barchart['>14 bis ≤17'] + 1
    elif value > 17 and value <= 20:
        barchart['>17 bis ≤20'] = barchart['>17 bis ≤20'] + 1
    elif value > 20 and value <= 40:
        barchart['>20 bis ≤40'] = barchart['>20 bis ≤40'] + 1
    elif value > 40 and value <= 80:
        barchart['>40 bis ≤80'] = barchart['>40 bis ≤80'] + 1
    elif value > 80 and value <= 160:
        barchart['>80 bis ≤160'] = barchart['>80 bis ≤160'] + 1



# make plot
plt.rc('xtick', labelsize='14')
plt.rc('ytick', labelsize='14')
fig2, ax2 = plt.subplots(figsize=(plotsize[0], plotsize[1]))
barobj = ax2.bar(barchart.keys(), barchart.values(), color='b')
ax2.set_ylabel('Anzahl der Grauwerte', size='18')
ax2.set_xlabel('Höhenkategorie in Meter', size='18')
# ax2.get_xaxis().get_major_formatter().set_scientific(False)
# ax2.ticklabel_format(useOffset=False, style='plain', axis='y')
autolabel(barobj)
plt.tight_layout()
# plt.show()
plt.savefig('berlinErgebnisseRaster.png', bbox_inches='tight')



values = None
barchart = None
##################################### Phoenix ####################################

ds = gdal.Open("./../../processing/output/phoenix/all_LOD.tif")
values = np.concatenate(np.array(ds.GetRasterBand(1).ReadAsArray()))
# # myarray = np.array([1,2,3,4,5,6])
# # print(myarray)
# # print(len(myarray))
# # print(myarray.max())
# # print(myarray.min())
#
#
barchart = {
    '2 bis ≤5': 0,
    '>5 bis ≤8': 0,
    '>8 bis ≤11': 0,
    '>11 bis ≤14': 0,
    '>14 bis ≤17': 0,
    '>17 bis ≤20': 0,
    '>20 bis ≤40': 0,
    '>40 bis ≤80': 0,
    '>80 bis ≤160': 0
}

for value in values:
    if value <= 5:
        barchart['2 bis ≤5'] = barchart['2 bis ≤5']+1
    elif value > 5 and value <= 8:
        barchart['>5 bis ≤8'] = barchart['>5 bis ≤8'] + 1
    elif value > 8 and value <= 11:
        barchart['>8 bis ≤11'] = barchart['>8 bis ≤11'] + 1
    elif value > 11 and value <= 14:
        barchart['>11 bis ≤14'] = barchart['>11 bis ≤14'] + 1
    elif value > 14 and value <= 17:
        barchart['>14 bis ≤17'] = barchart['>14 bis ≤17'] + 1
    elif value > 17 and value <= 20:
        barchart['>17 bis ≤20'] = barchart['>17 bis ≤20'] + 1
    elif value > 20 and value <= 40:
        barchart['>20 bis ≤40'] = barchart['>20 bis ≤40'] + 1
    elif value > 40 and value <= 80:
        barchart['>40 bis ≤80'] = barchart['>40 bis ≤80'] + 1
    elif value > 80 and value <= 160:
        barchart['>80 bis ≤160'] = barchart['>80 bis ≤160'] + 1

# make plot
plt.rc('xtick', labelsize='14')
plt.rc('ytick', labelsize='14')
fig2, ax2 = plt.subplots(figsize=(plotsize[0], plotsize[1]))
barobj = ax2.bar( barchart.keys(),barchart.values(), color='b')
ax2.set_ylabel('Anzahl der Grauwerte', size='18')
ax2.set_xlabel('Höhenkategorie in Meter', size='18')
# ax2.ticklabel_format(useOffset=False, style='plain', axis='y')
autolabel(barobj)
# plt.show()
plt.tight_layout()
plt.savefig('phoenixErgebnisseRaster.png', bbox_inches='tight')