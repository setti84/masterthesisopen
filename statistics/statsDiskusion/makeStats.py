import sys
import os
from osgeo import ogr, osr, gdal
import numpy as np
from scipy import stats
import re
import matplotlib.pyplot as plt
import statistics

gdal.UseExceptions()

######################### read file #############################


def getData():

    drv = ogr.GetDriverByName('GPKG')
    file = drv.Open(filepath, gdal.GA_ReadOnly)
    layer = file.GetLayer()

    feature = layer.GetNextFeature()

    x_arrow = []
    y_arrow = []

    while feature:
        x_value = feature.GetField(fields[0])
        y_value = feature.GetField(fields[1])

        if x_value is not None:
            x_arrow.append(round(float(x_value), 2))
        else:
            # raise Exception('check values x')
            print("check values x")

        if y_value is not None:
            y_arrow.append(round(float(y_value), 2))
        else:
            # raise Exception('check values y')
            print(y_value)
            print("check values y")

        feature = layer.GetNextFeature()

    data = [x_arrow, y_arrow]
    return data


##############################################################################################################

fileName = 'berlinRandom'
filepath = '/home/sebastian/Documents/masterArbeit/code/source-data/berlin/referenceData/' + fileName + '.gpkg'
fields = ['height', '_median']

plotsize = [13, 9]
stat_data = [
    [], # x values
    [], # y values
    [], # difference in meter
    []  # difference in procent
]

values = getData()
#ref height
stat_data[0] = values[0]
# calc height
stat_data[1] = values[1]
corr = np.corrcoef(stat_data[0], stat_data[1])[1, 0]


# scatterplot
fig, ax = plt.subplots(figsize=(9, 9)) # nrows=1, ncols=2,
ax.grid()
ax.set_yticks(np.arange(0, np.array(stat_data[1]).max() + 5, 5))
ax.set_xticks(np.arange(0, np.array(stat_data[1]).max() + 5, 5))
ax.scatter(stat_data[0], stat_data[1], alpha=0.5)
ax.set_xlabel('Referenzhöhe', size='large')
ax.set_ylabel('Berechnete Höhe', size='large')
ax.tick_params(axis='both', which='major', labelsize=11)
ax.axis([0, np.array(stat_data[1]).max() + 5, 0, np.array(stat_data[1]).max() + 5])
 # ax.set_title('Streudiagramm Berechnete Höhe und Referenzhöhe')
ax.text(2, np.array(stat_data[1]).max()+2, 'Korrelation: ' + str(round(corr, 3)), ha='left', wrap=True, size='large',
        bbox=dict(facecolor='none', edgecolor='green', boxstyle='round', ec=(1., 0.5, 0.5),
                   fc=(1., 0.8, 0.8),))
plt.tight_layout()
plt.savefig(fileName + '.png')
# plt.show()

fileName = None
filepath = None
fields = None
data = None
plotsize = None
fig = None

# ############################################################################################################
#

fileName = 'phoenixRandom'
filepath = '/home/sebastian/Documents/masterArbeit/code/source-data/phoenix/referenceData/' + fileName + '.gpkg'
fields = ['height', '_median']

plotsize = [13, 9]
stat_data = [
    [], # x values
    [], # y values
    [], # difference in meter
    []  # difference in procent
]

values = getData()
#ref height
stat_data[0] = values[0]
# calc height
stat_data[1] = values[1]
corr = np.corrcoef(stat_data[0], stat_data[1])[1, 0]


# scatterplot
fig, ax = plt.subplots(figsize=(9, 9)) # nrows=1, ncols=2,
ax.grid()
ax.set_yticks(np.arange(0, np.array(stat_data[1]).max() + 5, 5))
ax.set_xticks(np.arange(0, np.array(stat_data[1]).max() + 5, 5))
ax.scatter(stat_data[0], stat_data[1], alpha=0.5)
ax.set_xlabel('Referenzhöhe', size='large')
ax.set_ylabel('Berechnete Höhe', size='large')
ax.tick_params(axis='both', which='major', labelsize=11)
ax.axis([0, np.array(stat_data[1]).max() + 5, 0, np.array(stat_data[1]).max() + 5])
 # ax.set_title('Streudiagramm Berechnete Höhe und Referenzhöhe')
ax.text(2, np.array(stat_data[1]).max()+2, 'Korrelation: ' + str(round(corr, 3)), ha='left', wrap=True, size='large',
        bbox=dict(facecolor='none', edgecolor='green', boxstyle='round', ec=(1., 0.5, 0.5),
                   fc=(1., 0.8, 0.8),))
plt.tight_layout()
plt.savefig(fileName + '.png')
# plt.show()

fileName = None
filepath = None
fields = None
data = None
plotsize = None
fig = None

############################################################################################################

#
fileName = 'daressalaamRandom'
filepath = '/home/sebastian/Documents/masterArbeit/code/source-data/daressalaam/referenceData/' + fileName + '.gpkg'
fields = ['height', '_median']

plotsize = [13, 9]
stat_data = [
    [], # x values
    [], # y values
    [], # difference in meter
    []  # difference in procent
]

values = getData()
#ref height
stat_data[0] = values[0]
# calc height
stat_data[1] = values[1]
corr = np.corrcoef(stat_data[0], stat_data[1])[1, 0]


# scatterplot
fig, ax = plt.subplots(figsize=(9, 9)) # nrows=1, ncols=2,
ax.grid()
ax.set_yticks(np.arange(0, np.array(stat_data[1]).max() + 5, 5))
ax.set_xticks(np.arange(0, np.array(stat_data[1]).max() + 5, 5))
ax.scatter(stat_data[0], stat_data[1], alpha=0.5)
ax.set_xlabel('Referenzhöhe', size='large')
ax.set_ylabel('Berechnete Höhe', size='large')
ax.tick_params(axis='both', which='major', labelsize=11)
ax.axis([0, np.array(stat_data[1]).max() + 5, 0, np.array(stat_data[1]).max() + 5])
ax.text(2, np.array(stat_data[1]).max()+2, 'Korrelation: ' + str(round(corr, 3)), ha='left', wrap=True, size='large',
        bbox=dict(facecolor='none', edgecolor='green', boxstyle='round', ec=(1., 0.5, 0.5),
                   fc=(1., 0.8, 0.8),))
plt.tight_layout()
plt.savefig(fileName + '.png')
# plt.show()