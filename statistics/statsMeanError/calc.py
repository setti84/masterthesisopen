from osgeo import ogr, osr, gdal
import numpy as np
import matplotlib.pyplot as plt

gdal.UseExceptions()
print("hello User :)")

def getData(filepath):

    drv = ogr.GetDriverByName('GPKG')
    file = drv.Open(filepath, gdal.GA_ReadOnly)
    layer = file.GetLayer()

    feature = layer.GetNextFeature()
    referenced = []
    predicted = []

    while feature:
        raw_ref = feature.GetField('calc_median')
        raw_pre = feature.GetField('height')

        if raw_ref is not None and raw_pre is not None:
            referenced.append(raw_ref)
            predicted.append(raw_pre)
        else:
            print('check values')

        feature = layer.GetNextFeature()

    return [referenced, predicted]

# berlin
# ref_path = '/home/sebastian/Documents/masterArbeit/code/source-data/berlin/referenceData/berlin_referene_data.gpkg'
ref_path = '/home/sebastian/Documents/masterArbeit/code/source-data/berlin/referenceData/berlinRandom.gpkg'
values = getData(ref_path)

# get the height difference between predicted height and referenced height
raw_differences = []
for num, value in enumerate(values[0]):
    raw_differences.append(round(values[1][num]-value,2))

differences = np.array(raw_differences)
print(max(differences))

fig4, ax4 = plt.subplots(figsize=(13,9))
ax4.set_ylabel(' Höhenunterschied in Meter', size='18')
ax4.set_xlabel('Gebäude', size='18')
ax4.tick_params(axis='both', which='major', labelsize=14)
ax4.fill_between(np.arange(0, len(differences)), 0, differences, facecolor='blue', alpha=0.5)
plt.tight_layout()
plt.savefig('berlinRandom_meanError.png')
# plt.show()



# Phoenix
# ref_path = '/home/sebastian/Documents/masterArbeit/code/source-data/berlin/referenceData/berlin_referene_data.gpkg'
ref_path = '/home/sebastian/Documents/masterArbeit/code/source-data/phoenix/referenceData/phoenixRandom.gpkg'
values = getData(ref_path)

# get the height difference between predicted height and referenced height
raw_differences = []
for num, value in enumerate(values[0]):
    raw_differences.append(round(values[1][num]-value,2))

differences = np.array(raw_differences)
print(max(differences))

fig4, ax4 = plt.subplots(figsize=(13,9))
ax4.set_ylabel(' Höhenunterschied in Meter', size='18')
ax4.set_xlabel('Gebäude', size='18')
ax4.tick_params(axis='both', which='major', labelsize=14)
ax4.fill_between(np.arange(0, len(differences)), 0, differences, facecolor='blue', alpha=0.5)
plt.tight_layout()
plt.savefig('phoenixRandom_meanError.png')
# plt.show()



# Dar Es Salaam
ref_path = '/home/sebastian/Documents/masterArbeit/code/source-data/daressalaam/referenceData/daressalaamRandom.gpkg'
values = getData(ref_path)

# get the height difference between predicted height and referenced height
raw_differences = []
for num, value in enumerate(values[0]):
    raw_differences.append(round(values[1][num]-value, 2))

differences = np.array(raw_differences)
print(max(differences))

fig4, ax4 = plt.subplots(figsize=(13,9))
ax4.tick_params(axis='both', which='major', labelsize=14)
ax4.set_ylabel(' Höhenunterschied in Meter', size='18')
ax4.set_xlabel('Gebäude', size='18')
ax4.fill_between(np.arange(0, len(differences)), 0, differences, facecolor='blue', alpha=0.5)
plt.tight_layout()
plt.savefig('daressalaamRandom_meanError.png')
# plt.show()