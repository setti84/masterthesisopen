import sys
from osgeo import ogr, osr, gdal
import numpy as np
from scipy import stats
import re
import matplotlib.pyplot as plt
import statistics
import math

gdal.UseExceptions()

######################### read file #############################

def autolabel(barobj):
    """Attach a text label above each bar in *rects*, displaying its height."""
    for rect in barobj:
        height = rect.get_height()
        ax2.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom', size='14')

def getData():

    drv = ogr.GetDriverByName('GPKG')
    file = drv.Open(filepath, gdal.GA_ReadOnly)
    layer = file.GetLayer()


    feature = layer.GetNextFeature()
    x_arrow = []

    while feature:
        x_value = feature.GetField('height')

        if x_value is not None:
            x_arrow.append(round(float(x_value), 0))
        else:
            # raise Exception('check values x')
            print("check values x")

        feature = layer.GetNextFeature()

    return x_arrow


################################ Berlin reference data ####################################################
#
# fileName = 'berlin_referene_data' # berlinRandom5
#
# filepath = '/home/sebastian/Documents/masterArbeit/code/source-data/berlin/referenceData/' + fileName + '.gpkg'
# plotsize = [14, 8]
#
# try:
#     values = np.array(getData())
# except:
#     print("could not find data")
#
# # print(values)
# min = np.min(values)
# max = np.max(values)
# print(min, max)
#
#
# # get an geometrik scale
# # a = 3
# # r = 2
# # length = 10
# # geometric = [a * r ** (n - 1) for n in range(1, length + 1)]
# # print(geometric)
# # [3, 6, 12, 24, 48, 96, 192,
#
# barchart = {
#     '2 bis ≤5': 0,
#     '>5 bis ≤8': 0,
#     '>8 bis ≤11': 0,
#     '>11 bis ≤14': 0,
#     '>14 bis ≤17': 0,
#     '>17 bis ≤20': 0,
#     '>20 bis ≤40': 0,
#     '>40 bis ≤80': 0,
#     '>80 bis ≤160': 0
# }
#
# for value in values:
#     if value <= 5:
#         barchart['2 bis ≤5'] = barchart['2 bis ≤5']+1
#     elif value > 5 and value <= 8:
#         barchart['>5 bis ≤8'] = barchart['>5 bis ≤8'] + 1
#     elif value > 8 and value <= 11:
#         barchart['>8 bis ≤11'] = barchart['>8 bis ≤11'] + 1
#     elif value > 11 and value <= 14:
#         barchart['>11 bis ≤14'] = barchart['>11 bis ≤14'] + 1
#     elif value > 14 and value <= 17:
#         barchart['>14 bis ≤17'] = barchart['>14 bis ≤17'] + 1
#     elif value > 17 and value <= 20:
#         barchart['>17 bis ≤20'] = barchart['>17 bis ≤20'] + 1
#     elif value > 20 and value <= 40:
#         barchart['>20 bis ≤40'] = barchart['>20 bis ≤40'] + 1
#     elif value > 40 and value <= 80:
#         barchart['>40 bis ≤80'] = barchart['>40 bis ≤80'] + 1
#     elif value > 80 and value <= 160:
#         barchart['>80 bis ≤160'] = barchart['>80 bis ≤160'] + 1
#
#
# # make plot
# plt.rc('xtick', labelsize='14')
# plt.rc('ytick', labelsize='14')
# fig2, ax2 = plt.subplots(figsize=(plotsize[0], plotsize[1]))
# barobj2 = ax2.bar( barchart.keys(), barchart.values(), color='b')
# ax2.set_ylabel('Anzahl der Gebäude', size='18')
# ax2.set_xlabel('Höhenkategorie in Meter', size='18')
# autolabel(barobj2)
# plt.tight_layout()
# # plt.show()
# plt.savefig('berlinReferenceDataHeight.png', bbox_inches='tight')
#
#
#
# #############################    phenix reference data height  ##############################################
#
# fileName = 'phoenix_referene_data'
# filepath = '/home/sebastian/Documents/masterArbeit/code/source-data/phoenix/referenceData/' + fileName + '.gpkg'
#
# plotsize = [14,8]
#
# try:
#     values = np.array(getData())
# except:
#     print("could not find data")
#
# min = np.min(values)
# max = np.max(values)
# print(min, max)
#
# barchart = {
#     '2 bis ≤5': 0,
#     '>5 bis ≤8': 0,
#     '>8 bis ≤11': 0,
#     '>11 bis ≤14': 0,
#     '>14 bis ≤17': 0,
#     '>17 bis ≤20': 0,
#     '>20 bis ≤40': 0,
#     '>40 bis ≤80': 0,
#     '>80 bis ≤160': 0
# }
#
# for value in values:
#     if value <= 5:
#         barchart['2 bis ≤5'] = barchart['2 bis ≤5']+1
#     elif value > 5 and value <= 8:
#         barchart['>5 bis ≤8'] = barchart['>5 bis ≤8'] + 1
#     elif value > 8 and value <= 11:
#         barchart['>8 bis ≤11'] = barchart['>8 bis ≤11'] + 1
#     elif value > 11 and value <= 14:
#         barchart['>11 bis ≤14'] = barchart['>11 bis ≤14'] + 1
#     elif value > 14 and value <= 17:
#         barchart['>14 bis ≤17'] = barchart['>14 bis ≤17'] + 1
#     elif value > 17 and value <= 20:
#         barchart['>17 bis ≤20'] = barchart['>17 bis ≤20'] + 1
#     elif value > 20 and value <= 40:
#         barchart['>20 bis ≤40'] = barchart['>20 bis ≤40'] + 1
#     elif value > 40 and value <= 80:
#         barchart['>40 bis ≤80'] = barchart['>40 bis ≤80'] + 1
#     elif value > 80 and value <= 160:
#         barchart['>80 bis ≤160'] = barchart['>80 bis ≤160'] + 1
#
# print(len(values))
# print(barchart)
# # make plot
# plt.rc('xtick', labelsize='14')
# plt.rc('ytick', labelsize='14')
# fig2, ax2 = plt.subplots(figsize=(plotsize[0], plotsize[1]))
# barobj = ax2.bar( barchart.keys(),barchart.values(), color='b')
# ax2.set_ylabel('Anzahl der Gebäude', size='18')
# ax2.set_xlabel('Höhenkategorie in Meter', size='18')
# autolabel(barobj)
# # plt.show()
# plt.tight_layout()
# plt.savefig('phoenixReferenceDataHeight.png', bbox_inches='tight')



#############################    daressalaam reference data height  ##############################################

fileName = 'daresssalaam_reference_data'
filepath = '/home/sebastian/Documents/masterArbeit/code/source-data/daressalaam/referenceData/' + fileName + '.gpkg'

plotsize = [14,8]

try:
    values = np.array(getData())
except:
    print("could not find data")

min = np.min(values)
max = np.max(values)
print(min, max)

barchart = {
    '2 bis ≤5': 0,
    '>5 bis ≤8': 0,
    '>8 bis ≤11': 0,
    '>11 bis ≤14': 0,
    '>14 bis ≤17': 0,
    '>17 bis ≤20': 0,
    '>20 bis ≤40': 0,
    '>40 bis ≤80': 0,
    '>80 bis ≤160': 0
}

for value in values:
    if value <= 5:
        barchart['2 bis ≤5'] = barchart['2 bis ≤5']+1
    elif value > 5 and value <= 8:
        barchart['>5 bis ≤8'] = barchart['>5 bis ≤8'] + 1
    elif value > 8 and value <= 11:
        barchart['>8 bis ≤11'] = barchart['>8 bis ≤11'] + 1
    elif value > 11 and value <= 14:
        barchart['>11 bis ≤14'] = barchart['>11 bis ≤14'] + 1
    elif value > 14 and value <= 17:
        barchart['>14 bis ≤17'] = barchart['>14 bis ≤17'] + 1
    elif value > 17 and value <= 20:
        barchart['>17 bis ≤20'] = barchart['>17 bis ≤20'] + 1
    elif value > 20 and value <= 40:
        barchart['>20 bis ≤40'] = barchart['>20 bis ≤40'] + 1
    elif value > 40 and value <= 80:
        barchart['>40 bis ≤80'] = barchart['>40 bis ≤80'] + 1
    elif value > 80 and value <= 160:
        barchart['>80 bis ≤160'] = barchart['>80 bis ≤160'] + 1

print(len(values))
print(barchart)
# make plot
plt.rc('xtick', labelsize='14')
plt.rc('ytick', labelsize='14')
fig2, ax2 = plt.subplots(figsize=(plotsize[0], plotsize[1]))
barobj = ax2.bar( barchart.keys(),barchart.values(), color='b')
ax2.set_ylabel('Anzahl der Gebäude', size='18')
ax2.set_xlabel('Höhenkategorie in Meter', size='18')
autolabel(barobj)
# plt.show()
plt.tight_layout()
plt.savefig('daressalaamReferenceDataHeight.png', bbox_inches='tight')