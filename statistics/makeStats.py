import sys
from osgeo import ogr, osr, gdal
import numpy as np
from scipy import stats
import re
import matplotlib.pyplot as plt
import statistics

gdal.UseExceptions()

######################### read file #############################

def getData():

    drv = ogr.GetDriverByName('GPKG')
    file = drv.Open(filepath, gdal.GA_ReadOnly)
    layer = file.GetLayer()

    feature = layer.GetNextFeature()

    x_arrow = []
    y_arrow = []

    while feature:
        x_value = feature.GetField(fields[0])
        y_value = feature.GetField(fields[1])

        if x_value is not None:
            x_arrow.append(round(float(x_value), 2))
        else:
            # raise Exception('check values x')
            print("check values x")

        if y_value is not None:
            y_arrow.append(round(float(y_value), 2))
        else:
            # raise Exception('check values y')
            print("check values y")

        feature = layer.GetNextFeature()

    data = [x_arrow, y_arrow]
    return data

fileName = 'berlinRandom5' # berlinRandom5

filepath = '/home/sebastian/Documents/masterArbeit/code/source-data/berlin/referenceData/' + fileName + '.gpkg'
fields = ['meanHeight', '_median'] # _median _mean
plotsize = [13, 9]
stat_data = [
    [], # x values
    [], # y values
    [], # difference in meter
    []  # difference in procent
]

values = getData()
#ref height
stat_data[0] = values[0]
# calc height
stat_data[1] = values[1]
corr = np.corrcoef(stat_data[0], stat_data[1])[1, 0]

# calculate difference of reference height and calculated height in meter
diff_in_m = []
for index, x in enumerate(stat_data[0]):
    diff_in_m.append(abs(round(x - (stat_data[1][index]), 1)))
stat_data[2] = diff_in_m

# calculate difference of reference height and calculated height in procent
diff_in_proc = []
for index, x in enumerate(stat_data[0]):
    diff_in_proc.append(abs(round((stat_data[1][index]/x*100)-100, 2)))
stat_data[3] = diff_in_proc


################################################### plots ##############################################################

# scatterplot
fig, ax = plt.subplots(figsize=(9, 9)) # nrows=1, ncols=2,
ax.grid()
ax.set_title('Streudiagramm Höhenunterschiede', size='x-large')
ax.set_yticks(np.arange(0, np.array(stat_data[1]).max() + 5, 5))
ax.set_xticks(np.arange(0, np.array(stat_data[1]).max() + 5, 5))
ax.scatter(stat_data[0], stat_data[1], alpha=0.5)
ax.set_xlabel('Referenzhöhe')
ax.set_ylabel('Berechnete Höhe')
ax.axis([0, np.array(stat_data[1]).max() + 5, 0, np.array(stat_data[1]).max() + 5])
ax.set_title('Streudiagramm Berechnete Höhe und Referenzhöhe')
ax.text(2, np.array(stat_data[1]).max()+2, 'Korrelation: ' + str(round(corr, 3)), ha='left', wrap=True,
        bbox=dict(facecolor='none', edgecolor='green', boxstyle='round', ec=(1., 0.5, 0.5),
                   fc=(1., 0.8, 0.8),))
plt.savefig('st_dia_abs_' + fileName + '.png')
# plt.show()

#####################################################################################################
# diagramm für Unterschiede in meter

# sort statistical data for diff height in meter
temp_stat = list(zip(stat_data[2], stat_data[0], stat_data[1], stat_data[3]))
temp_stat.sort()
diff_height_m, ref_height, calc_height, diff_height_p = zip(*temp_stat)

# make plot
median = np.ones(len(diff_height_m)+1) * statistics.median(diff_height_m)
mean = np.ones(len(diff_height_m)+1) * statistics.mean(diff_height_m)
mode_prep = stats.mode(np.array(diff_height_m))
mode = np.ones(len(diff_height_m)+1) * mode_prep.mode
fig2, ax2 = plt.subplots(figsize=(plotsize[0], plotsize[1]))
ax2.set_title('Auswertung Höhenunterschiede', size='x-large')
ax2.grid()
ax2.set_yticks(np.arange(0, np.array(calc_height).max()+1, 5))
ax2.plot(np.arange(0.5, len(ref_height) + 0.5), calc_height, 'sg', label='Berechnete Höhe', linewidth=2)
ax2.plot(np.arange(0.5, len(ref_height) + 0.5), ref_height, 'sb', label='Referenzhöhe')
ax2.plot(np.arange(0, len(ref_height) + 1), mean, '-m', label='Mean', linewidth='2')
ax2.plot(np.arange(0, len(ref_height) + 1), median, '-c', label='Median', linewidth='2')
ax2.plot(np.arange(0, len(ref_height) + 1), mode, '-k', label='Mode', linewidth='2')
ax2.bar(np.arange(1, len(ref_height) + 1)-1/2, diff_height_m, 1, color='r', label='Fehler')
ax2.text(30, np.array(calc_height).max()-3,
         'Mean: ' + str(round(statistics.mean(diff_height_m), 2)) +
         '\n Median: ' + str(round(statistics.median(diff_height_m), 2)) +
         '\n Modal: ' + str(round(mode_prep.mode[0], 2)),
         ha='left', wrap=True,
        bbox=dict(facecolor='none', edgecolor='green', boxstyle='round', ec=(1., 0.5, 0.5),
                   fc=(1., 0.8, 0.8),))
ax2.set_ylabel('Unterschied in Meter')
ax2.set_xlabel('Gebäude')
ax2.legend()
plt.savefig('m-diff_abs_' + fileName + '.png')
# plt.show()
diff_height_m = ref_height = calc_height = diff_height_p = None

######################################################################################################
# diagramm für Unterschiede in Prozent

temp_stat = list(zip(stat_data[3], stat_data[0], stat_data[1], stat_data[2]))
temp_stat.sort()
diff_height_p, ref_height, calc_height, diff_height_m = zip(*temp_stat)

median = np.ones(len(diff_height_p)+1) * statistics.median(diff_height_p)
mean = np.ones(len(diff_height_p)+1) * statistics.mean(diff_height_p)
mode_prep = stats.mode(np.array(diff_height_p))
mode = np.ones(len(diff_height_p)+1) * mode_prep.mode

fig3, ax3 = plt.subplots(figsize=(plotsize[0], plotsize[1]))
ax3.set_title('Auswertung Höhenunterschiede', size='x-large')
ax3.grid()

ax3.plot(np.arange(0, len(ref_height) + 1), mean, '-m', label='Mean', linewidth='2')
ax3.plot(np.arange(0, len(ref_height) + 1), median, '-c', label='Median', linewidth='2')
ax3.plot(np.arange(0, len(ref_height) + 1), mode, '-k', label='Mode', linewidth='2')
ax3.bar(np.arange(0, len(ref_height)), diff_height_p, 1, color='r', label='Fehler')

ax3.text(40, 40, # np.array(diff_height_p).max()-3
         'Mean: ' + str(round(statistics.mean(diff_height_p), 2)) +
         '\n Median: ' + str(round(statistics.median(diff_height_p), 2)) +
         '\n Modal: ' + str(round(mode_prep.mode[0], 2)),
         ha='left', wrap=True,
        bbox=dict(facecolor='none', edgecolor='green', boxstyle='round', ec=(1., 0.5, 0.5),
                   fc=(1., 0.8, 0.8),))
ax3.set_ylabel('Unterschied in Prozent')
ax3.set_xlabel('Gebäude')
ax3.legend()
# plt.show()
plt.savefig('m-diff_abs_proc_' + fileName + '.png')
diff_height_m = ref_height = calc_height = diff_height_p = None

########################################################################################################

# Höhenunterschiede in Plus und Minus Richtung in Meter
diff_heights = []
for index, x in enumerate(stat_data[0]):
    diff_heights.append(round(x - (stat_data[1][index]), 1))

mean = np.ones(len(diff_heights)) * statistics.mean(diff_heights)

fig4, ax4 = plt.subplots(figsize=(plotsize[0], plotsize[1]))
ax4.set_title('Höhenunterschiede in Meter', size='x-large')
ax4.set_ylabel(' Höhenunterschied in Meter')
ax4.set_xlabel('Gebäude')
ax4.plot(np.arange(0, len(stat_data[2])), mean, color='green', linestyle='solid', label='Mean', linewidth='1')
ax4.fill_between(np.arange(0, len(stat_data[2])), 0, diff_heights, facecolor='blue', alpha=0.5)
ax4.axhline(y=0, color='k')
ax4.legend()
ax4.text(5,  np.array(diff_heights).max()-1,
         'Durchschnitt Höhendifferenz: ' + str(mean[0]),
         ha='left', wrap=True,
         bbox=dict(facecolor='none', edgecolor='green', boxstyle='round', ec=(1., 0.5, 0.5),
                   fc=(1., 0.8, 0.8),))
# plt.show()
plt.savefig('m-diff_' + fileName + '.png')

########################################################################################################

# Höhenunterschiede in Plus und Minus Richtung in Prozent

# diff_heights = []
# for index, x in enumerate(stat_data[0]):
#     diff_heights.append(round((stat_data[1][index]/x*100)-100, 2))
#
# # diff_in_proc.append(abs(round((stat_data[1][index]/x*100)-100, 2)))
#
# mean = np.ones(len(diff_heights)) * statistics.mean(diff_heights)
#
# fig5, ax5 = plt.subplots(figsize=(plotsize[0], plotsize[1]))
# ax5.set_title('Höhenunterschiede in Prozent', size='x-large')
# ax5.plot(np.arange(0, len(stat_data[2])), diff_heights)
# ax5.plot(np.arange(0, len(stat_data[2])), mean, color='magenta', linestyle='solid', label='Mean', linewidth='1')
# ax5.set_ylabel(' Höhenunterschied in Prozent')
# ax5.set_xlabel('Gebäude')
# ax5.axhline(y=0, color='k')
# ax5.legend()
# ax5.text(5,  np.array(diff_heights).max()-1,
#          'Durchschnitt Höhendifferenz in Prozent: ' + str(round(mean[0],2)),
#          ha='left', wrap=True,
#          bbox=dict(facecolor='none', edgecolor='green', boxstyle='round', ec=(1., 0.5, 0.5),
#                    fc=(1., 0.8, 0.8),))
# plt.savefig('m-diff_proc_' + fileName + '.png')
# # plt.show()

########################################################################################################








# diff = []
# for index, x in enumerate(x_values):
#     diff.append(abs(round(x - (y_values[index]), 1)))

# temp_values = []
# for index, x in enumerate(x_values):
#     temp_values.append({"calcHeight": x, "refHeight": y_values[index], "diff": abs(round(x - (y_values[index]), 1)), "diff_proc": round(y_values[index]/x*100,2)})


# sorted for "diff height
# all_values = temp_values # sorted(temp_values, key=lambda k: k['diff_proc']) # temp_values


# x_values = []
# y_values = []
# diff = []
# diff_proc = []
#
# for value in all_values:
#     x_values.append(value["calcHeight"])
#     y_values.append(value["refHeight"])
#     diff.append(value["diff"])
#     diff_proc.append(value["diff_proc"])


