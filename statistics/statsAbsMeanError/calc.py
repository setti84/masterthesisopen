import sys
import os
from osgeo import ogr, osr, gdal
import numpy as np
from scipy import stats
import re
import matplotlib.pyplot as plt
import statistics

gdal.UseExceptions()
print("hello")

def getData(filepath):

    drv = ogr.GetDriverByName('GPKG')
    file = drv.Open(filepath, gdal.GA_ReadOnly)
    layer = file.GetLayer()

    feature = layer.GetNextFeature()
    referenced = []
    predicted = []

    while feature:
        raw_ref = feature.GetField('calc_median')
        raw_pre = feature.GetField('height')

        if raw_ref is not None and raw_pre is not None:
            referenced.append(raw_ref)
            predicted.append(raw_pre)
        else:
            print('check values')

        feature = layer.GetNextFeature()

    return [referenced, predicted]

# berlin
ref_path = '/home/sebastian/Documents/masterArbeit/code/source-data/berlin/referenceData/berlin_referene_data.gpkg'
values = getData(ref_path)
meanAbsErr = np.mean(np.abs(np.array(values[0]) - np.array(values[1])))
print(meanAbsErr)


# # phoenix
ref_path = '/home/sebastian/Documents/masterArbeit/code/source-data/phoenix/referenceData/phoenix_referene_data.gpkg'
values = getData(ref_path)
meanAbsErr = np.mean(np.abs(np.array(values[0]) - np.array(values[1])))
print(meanAbsErr)





