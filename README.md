# **Abstract**

The increasing global population and the growing number of people living in urban regions show that
there is a need to make cities sustainable and livable as living space for people. A planning tool for
this are city models and free geodata. These can be used in a variety of ways in urban planning and
problem solving for urban space and allow various processes to be visualized and planning models
to be run through.

In the free OpenStreetMap project, is a lot of geospatial data that can be used by a wide variety
of interest groups. They are taken up by volunteers and can be used for analysis purposes and in
urban planning. Unfortunately, building data, which have a special value in city models, are rarely
available in three dimensions in OSM. Therefore, this master’s thesis deals with the question of
whether it is possible to obtain the information of the third dimension from the existing OSM data.
For this purpose, a method was developed and applied for three study areas Berlin, Phoenix and
Dar Es Salaam, which divides the study areas into blocks using OSM objects such as streets, railway
tracks and rivers. In these blocks, the heights for surrounding buildings were calculated using the
existing building heights using median formation and IDW methods. The result includes a complete
elevation model of a city in the form of a grid.

For the verification process reference data from the Senate Department for Urban Development
in Berlin, the USGS in Phoenix and manually recorded data for Dar Es Salaam were available. The
verification process shows a correlation ratio of 0.6 for Berlin, 0.137 for Phoenix and 0.199 for Dar Es
Salaam. Results from a calculation of the absolute mean error over all buildings from the reference
data shows a height of 4.4 meters for Berlin and 1.4 meters for Phoenix.
