#! /usr/bin/python3

import os
import sys
import requests
import zipfile

path = os.getcwd()

if not os.path.exists(path + '/temp/'):
    os.mkdir(path + '/temp/')

url = 'https://fbinter.stadt-berlin.de/fb/atom/LOD1/LoD1.zip'
r = requests.get(url, allow_redirects=True)
open(path + '/temp/source.zip', 'wb').write(r.content)

# extract data in folder
with zipfile.ZipFile(path + '/temp/source.zip', 'r') as zip_ref:
    zip_ref.extractall(path + '/temp/')

#loop over all files convert to gpkg
for entry in os.scandir(path + '/temp/'):
    if entry.path.endswith(".xml") and entry.is_file():
        print(entry.path)
        os.system('ogr2ogr berlin_temp.GPKG GMLAS:' + entry.path + ' -nlt CONVERT_TO_LINEAR -nlt MULTIPOLYGON -s_srs EPSG:25833 -a_srs EPSG:25833 -update -append -oo EXPOSE_METADATA_LAYERS=YES')

query = '"SELECT ' \
        'round(measuredheight,1) as height, ' \
        'storeysaboveground as level, ' \
        'round(ST_Area(GeometryN(lod1solid,1)),0) as area, ' \
        'ST_Simplify(GeometryN(lod1solid,1),0.5) as GEOMETRY ' \
        'FROM building ' \
        'WHERE lod1solid is not Null and measuredheight >= 2 and storeysaboveground >=1 and ' \
        'ST_Area(GeometryN(lod1solid,1)) >= 6 and isValid(GeometryN(lod1solid,1))"'

os.system('ogr2ogr berlin_referene_data.gpkg berlin_temp.GPKG -dialect SQLITE -nln building -nlt POLYGON -SQL ' + query)


