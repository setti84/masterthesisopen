#! /usr/bin/python3

import os
path = os.getcwd()

if not os.path.exists(path + '/temp/'):
    os.mkdir(path + '/temp/')

filetypes = ['dbf', 'prj', 'shp', 'shx']

for filetype in filetypes:
    os.system('wget ftp://rockyftp.cr.usgs.gov/vdelivery/Datasets/Staged/Elevation/Non_Standard_Contributed/NGA_US_Cities/Phoenix_AZ/Phoenix_20140930/VEC/BLD2/Phoenix_AZ_2014_buildings_2d.' + filetype + ' -O ./temp/phoenix.' + filetype)

os.system('ogr2ogr ./temp/temp1.gpkg ./temp/phoenix.shp -t_srs EPSG:32612 -nln building -nlt POLYGON')

query = '"SELECT ' \
        'round(AVGHT_M,1) as height, ' \
        'round(ST_Area(geom),0) as area, ' \
        'ST_Simplify(geom,0.5) as geom ' \
        'FROM building ' \
        'WHERE geom is not Null and AVGHT_M >= 2 and ' \
        'ST_Area(geom) >= 6 and isValid(geom)"'

os.system('ogr2ogr phoenix_referene_data.gpkg ./temp/temp1.gpkg -dialect SQLITE -nln building -sql ' + query)
