from city import City
from lod import Lod

from PyQt5.QtCore import QVariant

##################################################################################################################
minimum_required_buildings = 2
min_area_building = 6  # in square meter

# amount_bdg_ht = Amount of buildings with height in one area
# mean_bdg_ht = mean value of all buildings with height in this area
# diff_bdg_ht = difference between highest and shortest building in the area
# median_ht_bdg = median of the height of the buildings
# area_id = Id from the area the building is in
# amount_bdg = Amount of buildings with no height information

stat_fields_area = {
    "amount_ht_bdg": [QVariant.Int],
    "modal_ht_bdg": [QVariant.Int],
    "mean_ht_bdg": [QVariant.Double],
    "median_ht_bdg": [QVariant.Double],
    "diff_ht_bdg": [QVariant.Double],
    "highest_bdg": [QVariant.Int],
    "lowest_bdg": [QVariant.Int],
    "amount_bdg": [QVariant.Int],
    "calc_height": [QVariant.Double]
}

stat_fields_building = [
    ["area_id", QVariant.Int]
]

idw_options = {
    "max_idw_features": 100,
    "min_idw_features": 2
}






##################################################################################################################

berlin = City(
    'Berlin',
    './../../source-data/berlin/berlin-latest.osm.pbf',
    './../../source-data/berlin/berlin-latest.gpkg',
    './../output/berlin/buildings.gpkg',
    './../output/berlin/buildings_height.gpkg',
    './../output/berlin/lines.gpkg',
    './../output/berlin/idw/',
    [
        Lod(1, './../output/berlin/LOD1.gpkg', None),
        Lod(2, './../output/berlin/LOD2.gpkg', """ "waterway" like 'canal' or "waterway" like 'river' or "highway" like 'trunk' or "highway" like 'trunk_link' or "highway" like 'primary_link' or "highway" like 'primary' or "highway" like 'motorway_link' or "highway" like 'motorway' or "other_tags" like '%"railway"=>"rail"%' or "other_tags" like '%"railway"=>"light_rail"%' """),
        Lod(3, './../output/berlin/LOD3.gpkg', """ "highway" like 'secondary' or "highway" like 'tertiary' """),
        Lod(4, './../output/berlin/LOD4.gpkg', """ "highway" like 'residential' or "highway" like 'unclassified' or "highway" like 'living_street' """)
    ],
    1,
    minimum_required_buildings,
    [
        'allotment_house', 'apartments', 'arena', 'bell_tower', 'bungalow', 'car_wash', 'cathedral', 'chapel', 'chimney',
        'church', 'cinema', 'civic', 'clinic', 'college', 'commercial', 'concert_hall', 'conservatory', 'courthouse',
        'datscha', 'detached', 'dormitory', 'dwelling_house', 'electricity', 'embassy', 'exhibition_hall', 'farm_auxiliary',
        'fire_station', 'garage', 'garages', 'gas_station', 'gatehouse', 'government', 'greenhouse',
        'guardhouse', 'gym', 'hall', 'hangar', 'hospital', 'hostel', 'hotel', 'house', 'hut', 'industrial', 'kindergarten',
        'kiosk', 'lab', 'leisure', 'library', 'mall', 'mansion', 'manufacture', 'military', 'mosque', 'museum', 'office',
        'opera_house', 'parking', 'pharmacy', 'power', 'public', 'public_building', 'railway', 'religious', 'residential',
        'retail', 'retirement_home', 'riding_hall', 'roof', 'school', 'semidetached_house', 'shed', 'shop',
        'silo', 'sports_centre', 'sports_hall', 'stadium', 'station', 'storage_tank', 'substation', 'supermarket',
        'synagogue', 'tank', 'terrace', 'theatre', 'toilets', 'train_station', 'transportation', 'university',
        'viaduct', 'villa', 'warehouse', 'water_tower', 'yes'
    ],
    25833,
    stat_fields_area,
    stat_fields_building,
    min_area_building,
    idw_options,
    './../output/berlin/',
    './../../source-data/berlin/referenceData/berlin_referene_data.gpkg',
    './../../source-data/berlin/referenceData/berlinRandom.gpkg'
)

##################################################################################################################

#  './../../source-data/phoenix/arizona-latest-clipped.gpkg',

phoenix = City(
    'Phoenix',
    './../../source-data/phoenix/arizona-latest.osm.pbf',
    './../../source-data/phoenix/arizona-latest.gpkg',
    './../output/phoenix/buildings.gpkg',
    './../output/phoenix/buildings_height.gpkg',
    './../output/phoenix/lines.gpkg',
    './../output/phoenix/idw/',
    [
        Lod(1, './../output/phoenix/LOD1.gpkg', None),
        Lod(2, './../output/phoenix/LOD2.gpkg', """ "waterway" like 'canal' or "waterway" like 'river' or "highway" like 'trunk' or "highway" like 'trunk_link' or "highway" like 'primary_link' or "highway" like 'primary' or "highway" like 'motorway_link' or "highway" like 'motorway' or "other_tags" like '%"railway"=>"rail"%' or "other_tags" like '%"railway"=>"light_rail"%' """),
        Lod(3, './../output/phoenix/LOD3.gpkg', """ "highway" like 'secondary' or "highway" like 'tertiary' """),
        Lod(4, './../output/phoenix/LOD4.gpkg', """ "highway" like 'residential' or "highway" like 'unclassified' or "highway" like 'living_street' """)
    ],
    1,
    minimum_required_buildings,
    [
        'allotment_house', 'apartments', 'arena', 'bell_tower', 'bungalow', 'car_wash', 'cathedral', 'chapel',
        'chimney',
        'church', 'cinema', 'civic', 'clinic', 'college', 'commercial', 'concert_hall', 'conservatory', 'courthouse',
        'datscha', 'detached', 'dormitory', 'dwelling_house', 'electricity', 'embassy', 'exhibition_hall',
        'farm_auxiliary',
        'fire_station', 'garage', 'garages', 'gas_station', 'gatehouse', 'government', 'greenhouse',
        'guardhouse', 'gym', 'hall', 'hangar', 'hospital', 'hostel', 'hotel', 'house', 'hut', 'industrial',
        'kindergarten',
        'kiosk', 'lab', 'leisure', 'library', 'mall', 'mansion', 'manufacture', 'military', 'mosque', 'museum',
        'office',
        'opera_house', 'parking', 'pharmacy', 'power', 'public', 'public_building', 'railway', 'religious',
        'residential',
        'retail', 'retirement_home', 'riding_hall', 'roof', 'school', 'semidetached_house', 'service', 'shed', 'shop',
        'silo', 'sports_centre', 'sports_hall', 'stadium', 'station', 'storage_tank', 'substation', 'supermarket',
        'synagogue', 'tank', 'terrace', 'theatre', 'toilets', 'tower', 'train_station', 'transportation', 'university',
        'viaduct', 'villa', 'warehouse', 'water_tower', 'yes'
    ],
    32612,
    stat_fields_area,
    stat_fields_building,
    min_area_building,
    idw_options,
    './../output/phoenix/',
    './../../source-data/phoenix/referenceData/phoenix_referene_data.gpkg',
    './../../source-data/phoenix/referenceData/phoenixRandom.gpkg'
)

# ##################################################################################################################

# './../../source-data/daressalaam/tanzania-latest-clipped.gpkg',


daressalaam = City(
    'DarEsSalaam',
    './../../source-data/daressalaam/tanzania-latest.osm.pbf',
    './../../source-data/daressalaam/tanzania-latest.gpkg',
    './../output/daressalaam/buildings.gpkg',
    './../output/daressalaam/buildings_height.gpkg',
    './../output/daressalaam/lines.gpkg',
    './../output/daressalaam/idw/',
    [
        Lod(1, './../output/daressalaam/LOD1.gpkg', None),
        Lod(2, './../output/daressalaam/LOD2.gpkg', """ "waterway" like 'canal' or "waterway" like 'river' or "highway" like 'trunk' or "highway" like 'trunk_link' or "highway" like 'primary_link' or "highway" like 'primary' or "highway" like 'motorway_link' or "highway" like 'motorway' or "other_tags" like '%"railway"=>"rail"%' or "other_tags" like '%"railway"=>"light_rail"%' """),
        Lod(3, './../output/daressalaam/LOD3.gpkg', """ "highway" like 'secondary' or "highway" like 'tertiary' """),
        Lod(4, './../output/daressalaam/LOD4.gpkg', """ "highway" like 'residential' or "highway" like 'unclassified' or "highway" like 'living_street' """)
    ],
    1,
    minimum_required_buildings,
    [
        'allotment_house', 'apartments', 'arena', 'bell_tower', 'bungalow', 'car_wash', 'cathedral', 'chapel',
        'chimney',
        'church', 'cinema', 'civic', 'clinic', 'college', 'commercial', 'concert_hall', 'conservatory', 'courthouse',
        'datscha', 'detached', 'dormitory', 'dwelling_house', 'electricity', 'embassy', 'exhibition_hall',
        'farm_auxiliary',
        'fire_station', 'garage', 'garages', 'gas_station', 'gatehouse', 'government', 'greenhouse',
        'guardhouse', 'gym', 'hall', 'hangar', 'hospital', 'hostel', 'hotel', 'house', 'hut', 'industrial',
        'kindergarten',
        'kiosk', 'lab', 'leisure', 'library', 'mall', 'mansion', 'manufacture', 'military', 'mosque', 'museum',
        'office',
        'opera_house', 'parking', 'pharmacy', 'power', 'public', 'public_building', 'railway', 'religious',
        'residential',
        'retail', 'retirement_home', 'riding_hall', 'roof', 'school', 'semidetached_house', 'service', 'shed', 'shop',
        'silo', 'sports_centre', 'sports_hall', 'stadium', 'station', 'storage_tank', 'substation', 'supermarket',
        'synagogue', 'tank', 'terrace', 'theatre', 'toilets', 'tower', 'train_station', 'transportation', 'university',
        'viaduct', 'villa', 'warehouse', 'water_tower', 'yes'
    ],
    32737,
    stat_fields_area,
    stat_fields_building,
    min_area_building,
    idw_options,
    './../output/daressalaam/',
    './../../source-data/daressalaam/referenceData/daressalaam_referene_data.gpkg',
    './../../source-data/daressalaam/referenceData/daressalaamRandom.gpkg'
)
