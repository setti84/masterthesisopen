
import sys
import os
import re

from qgis.core import (
     QgsApplication,
     QgsVectorLayer,
     QgsVectorFileWriter,
     QgsCoordinateTransformContext,
     QgsField,
     QgsFeatureRequest
)

from qgis.analysis import QgsNativeAlgorithms
from PyQt5.QtCore import QVariant

QgsApplication.setPrefixPath('/usr', True)
qgs = QgsApplication([], False)
qgs.initQgis()
sys.path.append('/usr/share/qgis/python/plugins')

import processing
from processing.core.Processing import Processing
Processing.initialize()
QgsApplication.processingRegistry().addProvider(QgsNativeAlgorithms())
from processing.tools import dataobjects


def clipSource(source, clip, output):
    print(source, clip, output)
    clip = QgsVectorLayer(clip, "ogr")

    temp2 = processing.run("native:clip", {
        'INPUT': QgsVectorLayer(source + '|layername=points', "ogr"),
        'OVERLAY': clip,
        'OUTPUT':  'memory:points'
    })["OUTPUT"]

    temp3 = processing.run("native:clip", {
        'INPUT': QgsVectorLayer(source + '|layername=lines', "ogr"),
        'OVERLAY': clip,
        'OUTPUT': 'memory:lines'
    })["OUTPUT"]
    temp4 = processing.run("native:clip", {
        'INPUT': QgsVectorLayer(source + '|layername=multipolygons', "ogr"),
        'OVERLAY': clip,
        'OUTPUT': 'memory:multipolygons'
    })["OUTPUT"]

    save_options = QgsVectorFileWriter.SaveVectorOptions()
    save_options.driverName = "GPKG"
    save_options.layerName = "points"
    error = QgsVectorFileWriter.writeAsVectorFormatV2(temp2, output[:-5], QgsCoordinateTransformContext(), save_options)
    if error[0] == QgsVectorFileWriter.NoError:
        print("writing successful!")
    else:
        print(error)

    save_options.actionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteLayer
    save_options.layerName = "lines"
    error = QgsVectorFileWriter.writeAsVectorFormatV2(temp3, output[:-5], QgsCoordinateTransformContext(), save_options)
    if error[0] == QgsVectorFileWriter.NoError:
        print("writing successful!")
    else:
        print(error)

    save_options.layerName = "multipolygons"
    error = QgsVectorFileWriter.writeAsVectorFormatV2(temp4, output[:-5], QgsCoordinateTransformContext(), save_options)
    if error[0] == QgsVectorFileWriter.NoError:
        print("writing successful!")
    else:
        print(error)


def saveFileToDisk(layer, path):
    save_options = QgsVectorFileWriter.SaveVectorOptions()
    save_options.driverName = "GPKG"
    error = QgsVectorFileWriter.writeAsVectorFormatV2(
        layer, path, QgsCoordinateTransformContext(), save_options)
    if error[0] == QgsVectorFileWriter.NoError:
        print("writing successful!")
    else:
        print(error)


def prepareBuildings(sourcefile, layername, buildings_values, buildingsPath, buildingsHeightPath, min_area_building):
    no_buildings = []
    unusedFields = [
        "aeroway", "amenity", "admin_level", "barrier", "boundary", "craft", "geological",
        "height", "historic", "landuse", "land_area", "leisure", "military",
        "natural", "office", "other_tags", "place", "shop", "sport", "tourism", "type"
    ]

    print("Extract Buildings")
    temp1 = processing.run("native:extractbyexpression", {
        'INPUT': sourcefile + '|layername=' + layername,
        'EXPRESSION': """ "building" is not null """,
        'OUTPUT': 'memory:temp1'})

    temp2 = processing.run("native:fixgeometries", {
        'INPUT': temp1["OUTPUT"],
        'OUTPUT': 'memory:temp2'
    })

    print("calc area and filter for min area")
    temp3 = processing.run("qgis:fieldcalculator", {
        'INPUT': temp2["OUTPUT"],
        'FIELD_NAME': "area", 'FIELD_TYPE': 1, 'NEW_FIELD': True,
        'FORMULA': 'to_int($area)', 'OUTPUT': 'memory:temp3'})

    temp4 = processing.run("qgis:fieldcalculator", {
        'INPUT': temp3["OUTPUT"],
        'FIELD_NAME': "perimeter", 'FIELD_TYPE': 1, 'NEW_FIELD': True,
        'FORMULA': 'to_int($perimeter)', 'OUTPUT': 'memory:temp4'})

    temp5 = processing.run("native:extractbyexpression", {
        'INPUT': temp4["OUTPUT"],
        'EXPRESSION': ' $area > ' + str(min_area_building),
        'OUTPUT': 'memory:temp5'
    })

    print("Extract Buildings with height")
    temp6 = processing.run("native:extractbyexpression", {
        'INPUT': temp5["OUTPUT"],
        'EXPRESSION': """ "height" is not null or "level" is not null or "other_tags" like '%building:levels%' """,
        'OUTPUT': 'memory:temp6',
        'FAIL_OUTPUT': 'memory:temp7'
    })

    buildingsWithHeight = temp6["OUTPUT"]
    buildingsWithoutHeight = temp6["FAIL_OUTPUT"]

    # height buildings
    addField(buildingsWithHeight, QgsField("extHeight", QVariant.Double))
    no_buildings = extractBuildingTypes(buildingsWithHeight, buildings_values, no_buildings)
    # get the height information for buildings with height
    height_extraction(buildingsWithHeight)
    delete_fields(buildingsWithHeight, unusedFields)
    saveFileToDisk(buildingsWithHeight, buildingsHeightPath[:-5])

    # buildings without height
    delete_fields(buildingsWithoutHeight, unusedFields)
    addField(buildingsWithoutHeight, QgsField("calcHeight", QVariant.Double))
    no_buildings = extractBuildingTypes(buildingsWithoutHeight, buildings_values, no_buildings)

    saveFileToDisk(buildingsWithoutHeight, buildingsPath[:-5])
    no_buildings.sort()
    print("building type deleted: " + str(no_buildings))


def extractBuildingTypes(layer, buildings_values, no_buildings):

    man_made_values = ["communications_tower", "tower", "silo", "water_tower", "chimney", "storage_tank", "gasometer"]
    layer.startEditing()
    features = layer.getFeatures()

    for feature in features:
        raw_building = feature.attribute("building")
        raw_man_made = feature.attribute("man_made")

        if type(raw_man_made) == str and raw_man_made in man_made_values:
            layer.deleteFeature(feature.id())
        if type(raw_building) == str and raw_building not in buildings_values:
            # delete this building
            layer.deleteFeature(feature.id())
            # write this building type to deleted types
            if raw_building not in no_buildings:
                no_buildings.append(raw_building)

    layer.commitChanges()
    return no_buildings


def addField(layer, field):
    layer.startEditing()
    layer.addAttribute(field)
    layer.updateFields()
    layer.commitChanges()


def findHeight(layer, feature):

    old_height = str(feature.attribute("height"))
    old_level = str(feature.attribute("other_tags"))
    height = None
    if old_level is not 'NULL':
        try:
            tempLev = re.findall('building:levels"=>"[0-9]+"', old_level)
            level = int(tempLev[0][19:len(tempLev[0]) - 1])
            if level >= 1:
                height = float(level * 3)
        except:
            message = 'level conversation did not work: ' + str(tempLev)
            # print(message)
    if old_height is not 'NULL':
        try:
            tempHeight = re.findall('^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)', old_height)
            heightNew = float(tempHeight[0][0])
            if heightNew >= 2.0:
                height = float(heightNew)
        except:
            message = 'height conversation did not work: ' + str(height)
            # print(message)
    if height is None:
        layer.deleteFeature(feature.id())
    else:
        layer.changeAttributeValue(feature.id(), feature.fields().indexOf("extHeight"), height)


def height_extraction(layer):

    layer.startEditing()
    features = layer.getFeatures()

    for feature in features:
        findHeight(layer, feature)

    layer.commitChanges()


def delete_fields(layer, unused_fields):

    # delete fields which are not interesting
    unusedFieldsIndex = []
    fields = layer.dataProvider().fields()
    for unusedField in unused_fields:
        if unusedField in fields.names():
            unusedFieldsIndex.append(fields.indexFromName(unusedField))
    layer.dataProvider().deleteAttributes(unusedFieldsIndex)

    layer.updateFields()
    layer.commitChanges()


def prepareHeightField(source, type):

    temp = QgsVectorLayer(source + '|layername=' + type, "ogr")
    temp.startEditing()
    temp.addAttribute(QgsField("height", QVariant.Double))
    temp.updateFields()

    features = temp.getFeatures()
    for feature in features:
        raw_height = feature.attribute("other_tags")
        if raw_height is not 'NULL':
            try:
                tempheight = re.findall('height"=>"[0-9]+"', raw_height)
                height = float(tempheight[0][10:len(tempheight[0]) - 1])
                if height >= 2.0:
                    temp.changeAttributeValue(feature.id(), feature.fields().indexOf("height"), height)
            except:
                message = 'height conversation did not work: ' + str(raw_height)

    temp.commitChanges()