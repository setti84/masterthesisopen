
import sys
import os
from datetime import datetime
from osgeo import gdal
sys.path.append(os.path.abspath('../'))
from config import phoenix as p
from util.functions import prepareBuildings, prepareHeightField, clipSource

###############################################################################################################

startTime = datetime.now()
gdal.UseExceptions()

################################################################################################################


if os.path.isfile(p.origin_source):
    print("Origin source found: " + p.origin_source)
else:
    print("Origin sourcefile " + p.origin_source + " not found")
    print("Download " + p.origin_source)
    os.system('wget https://download.geofabrik.de/north-america/us/arizona-latest.osm.pbf --directory-prefix=./phoenix/ -O ' + p.origin_source)

if os.path.isfile(p.source):
    print("Source found: " + p.source)
else:
    print("Sourcefile " + p.source + " not found")
    print("Create Source " + p.source)
    os.system('ogr2ogr -f GPKG ./../../source-data/phoenix/temp.gpkg ' + p.origin_source + ' -t_srs EPSG:' + str(p.epsg) + ' -gt 200000')
    clipSource('./../../source-data/phoenix/temp.gpkg', p.lods[0].file, p.source)

print("Next...")
if os.path.isfile(p.buildings_height) and os.path.isfile(p.buildings):
    print("buildings found: " + p.buildings)
    print("buildings with height found: " + p.buildings_height)
else:
    print("buildings or buildings with height not found: generate...")
    prepareHeightField(p.source, 'multipolygons')
    prepareBuildings(p.source, 'multipolygons', p.building_values, p.buildings, p.buildings_height, p.min_area_building)
    print("buildings an buildings with height generated.")

print("Next...")
if os.path.isfile(p.lines):
    print("lines found: " + p.lines)
else:
    print("lines not found: " + p.lines + " generate...")
    gdal.VectorTranslate(p.lines, p.source, format='GPKG', where=' "highway" is not null or "waterway" is not null or "railway" is not null ', layers='lines')
    print("lines generated: " + p.lines)

print("preperation done...")
print("overall time: " + str(datetime.now() - startTime))
print("---------------------")