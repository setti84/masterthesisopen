
import sys
import os
from datetime import datetime
from osgeo import gdal
sys.path.append(os.path.abspath('../'))
from config import berlin as b
from util.functions import prepareBuildings, prepareHeightField

startTime = datetime.now()
gdal.UseExceptions()

#########################################################################################################

if os.path.isfile(b.origin_source):
    print("Origin source found: " + b.origin_source)
else:
    print("Origin sourcefile " + b.origin_source + " not found")
    print("Download " + b.origin_source)
    os.system('wget https://download.geofabrik.de/europe/germany/berlin-latest.osm.pbf --directory-prefix=./berlin/ -O ' + b.origin_source)

if os.path.isfile(b.source):
    print("Source found: " + b.source)
else:
    print("Sourcefile " + b.source + " not found")
    print("Create Source " + b.source)
    os.system('ogr2ogr -f GPKG ' + b.source + ' ' + b.origin_source + ' -t_srs EPSG:' + str(b.epsg) + ' -gt 200000')

if os.path.isfile(b.buildings_height) and os.path.isfile(b.buildings):
    print("buildings found: " + b.buildings)
    print("buildings with height found: " + b.buildings_height)
else:
    print("buildings or buildings with height not found: generate...")
    # the height field for buildings must be created
    prepareHeightField(b.source, 'multipolygons')
    #
    prepareBuildings(b.source, 'multipolygons', b.building_values, b.buildings, b.buildings_height, b.min_area_building)
    print("buildings an buildings with height generated.")

if os.path.isfile(b.lines):
    print("lines found: " + b.lines)
else:
    print("lines not found: " + b.lines + " generate...")
    gdal.VectorTranslate(b.lines, b.source, format='GPKG', where=' "highway" is not null or "waterway" is not null or "railway" is not null ', layers='lines')
    print("lines generated: " + b.lines)

print("preperation done...")
print("overall time: " + str(datetime.now() - startTime))
print("---------------------")
