
import sys
import os
from datetime import datetime
from osgeo import gdal
sys.path.append(os.path.abspath('../'))
from config import daressalaam as d
from util.functions import prepareBuildings, prepareHeightField, clipSource

startTime = datetime.now()
gdal.UseExceptions()

if os.path.isfile(d.origin_source):
    print("Origin source found: " + d.origin_source)
else:
    print("Origin sourcefile " + d.origin_source + " not found")
    print("Download " + d.origin_source)
    os.system('wget https://download.geofabrik.de/africa/tanzania-latest.osm.pbf --directory-prefix=./daressalaam/ -O ' + d.origin_source)

if os.path.isfile(d.source):
    print("Source found: " + d.source)
else:
    print("Sourcefile " + d.source + " not found")
    print("Create Source " + d.source)
    os.system('ogr2ogr -f GPKG ./../../source-data/daressalaam/temp.gpkg ' + d.origin_source + ' -t_srs EPSG:' + str(d.epsg) + ' -gt 200000 -nln points -nlt POINT -dialect SQLite -sql "select * from points where isValid(Geometry) = 1 " ')
    os.system('ogr2ogr -f GPKG ./../../source-data/daressalaam/temp.gpkg ' + d.origin_source + ' -t_srs EPSG:' + str(d.epsg) + ' -update -append -gt 200000 -nln lines -nlt LINESTRING -dialect SQLite -sql "select * from lines where isValid(Geometry) = 1 " ')
    os.system('ogr2ogr -f GPKG ./../../source-data/daressalaam/temp.gpkg ' + d.origin_source + ' -t_srs EPSG:' + str(d.epsg) + ' -update -append -gt 200000 -nln multipolygons -nlt MULTIPOLYGON -dialect SQLite -sql "select * from multipolygons where isValid(Geometry) = 1 " ')
    clipSource('./../../source-data/daressalaam/temp.gpkg', d.lods[0].file, d.source)

###########################################################################################

# if os.path.isfile(d.origin_source):
#     print("origin source found:  " + d.origin_source)
# else:
#     print("origin sourcefile " + d.origin_source + " not found")
#     print("stop...")
#     sys.exit()
#
# print("Next...")
# if os.path.isfile(d.source):
#     print("source found:        " + d.source)
# else:
#     print("sourcefile not found: " + d.source + " generate...")
#     clipSource(d.origin_source, d.lods[0].file, d.source)
#
print("Next...")
if os.path.isfile(d.buildings_height) and os.path.isfile(d.buildings):
    print("buildings found: " + d.buildings)
    print("buildings with height found: " + d.buildings_height)
else:
    print("buildings or buildings with height not found: generate...")
    prepareHeightField(d.source, 'multipolygons')
    prepareBuildings(d.source, 'multipolygons', d.building_values, d.buildings, d.buildings_height, d.min_area_building)
    print("buildings an buildings with height generated.")

print("Next...")
if os.path.isfile(d.lines):
    print("lines found: " + d.lines)
else:
    print("lines not found: " + d.lines + " generate...")
    gdal.VectorTranslate(d.lines, d.source, format='GPKG', where=' "highway" is not null or "waterway" is not null or "railway" is not null ', layers='lines')
    print("lines generated: " + d.lines)

print("preperation done...")
print("overall time: " + str(datetime.now() - startTime))
print("---------------------")
