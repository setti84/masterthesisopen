
import sys
import os
import re

from qgis.core import (
     QgsApplication,
     QgsVectorLayer,
     QgsCoordinateReferenceSystem,
     QgsVectorFileWriter,
     QgsCoordinateTransformContext,
     QgsSpatialIndex,
     QgsField,
     QgsFeature,
     QgsFeatureRequest
)

from qgis.analysis import QgsNativeAlgorithms

QgsApplication.setPrefixPath('/usr', True)
qgs = QgsApplication([], False)
qgs.initQgis()
sys.path.append('/usr/share/qgis/python/plugins')

import processing
from processing.core.Processing import Processing
Processing.initialize()
QgsApplication.processingRegistry().addProvider(QgsNativeAlgorithms())
from PyQt5.QtCore import QVariant
from processing.tools import dataobjects

#######################################################################################################################


def createMasterArea(source, target_area, Counter):

    temp1 = processing.run("native:extractbyexpression", {
        'INPUT': QgsVectorLayer(source + '|layername=lines', "ogr"),
        'EXPRESSION': """ "other_tags" like '%"admin_level"=>"4"%' """,
        'OUTPUT': 'memory:temp1'})

    temp2 = processing.run("qgis:polygonize", {'INPUT': temp1["OUTPUT"], 'KEEP_FIELDS': True, 'OUTPUT': 'memory:temp2'})

    temp3 = processing.run("native:simplifygeometries", {
        'INPUT': temp2["OUTPUT"],
        'METHOD': 0,
        'TOLERANCE': 10,
        'OUTPUT': 'memory:temp3'
    })

    temp4 = processing.run("qgis:deletecolumn", {
        'INPUT': temp3["OUTPUT"],
        'COLUMN': ['barrier', 'highway', 'waterway', 'aerialway', 'man_made', 'z_order', 'other_tags', 'osm_id', 'name'],
        'OUTPUT': 'memory:temp4'})

    temp5 = temp4["OUTPUT"]
    temp5.startEditing()
    area_id_field = QgsField("area_id", QVariant.Int)
    temp5.addAttribute(area_id_field)
    temp5.updateFields()

    features = temp5.getFeatures()
    for feature in features:
        temp5.changeAttributeValue(feature.id(), feature.fields().indexOf("area_id"), Counter.number)
        Counter.increase()
    temp5.commitChanges()
    saveFileToDisk(temp5, target_area)

def filter_subareas(buildingsindex, buildingslayer, areaslayer, min_req_bui):
    delete = []
    areas = areaslayer.getFeatures()

    for area in areas:
        intersect = buildingsindex.intersects(area.geometry().boundingBox())
        if len(intersect) < min_req_bui:
            delete.append(area.id())
        else:
            buildings_temp = 0
            for featureId in intersect:
                if area.geometry().intersects(buildingslayer.getFeature(featureId).geometry()):
                    buildings_temp += 1
                if buildings_temp >= min_req_bui:
                    break
            if buildings_temp < min_req_bui:
                delete.append(area.id())

    areaslayer.startEditing()
    # print(delete)
    for area in delete:
        areaslayer.deleteFeature(area)
    areaslayer.commitChanges()


def createsubareas(feature, lines_raw, options):

    temp1 = QgsVectorLayer('MultiPolygon?crs=epsg:' + str(options["epsg_code"]), 'temp1', "memory")
    dataprovider = temp1.dataProvider()
    dataprovider.addFeatures([feature])
    temp1.updateExtents()
    #
    # print(lines_raw)
    # print(temp1)
    #
    # save_options = QgsVectorFileWriter.SaveVectorOptions()
    # save_options.driverName = "GPKG"
    # error = QgsVectorFileWriter.writeAsVectorFormatV2(lines_raw, 'testi.gpkg', QgsCoordinateTransformContext(), save_options)
    # if error[0] == QgsVectorFileWriter.NoError:
    #     print("writing successful!")
    # else:
    #     print(error)

    # save_options = QgsVectorFileWriter.SaveVectorOptions()
    # save_options.driverName = "GPKG"
    # error = QgsVectorFileWriter.writeAsVectorFormatV2(temp1, 'testi2.gpkg', QgsCoordinateTransformContext(),
    #                                                   save_options)
    # if error[0] == QgsVectorFileWriter.NoError:
    #     print("writing successful!")
    # else:
    #     print(error)

    # sys.exit()


    temp2 = processing.run("native:clip", { 'INPUT': lines_raw, 'OVERLAY': temp1, 'OUTPUT': 'memory:temp2'})

    temp3 = processing.run("native:extractbyexpression", {
        'INPUT': temp2["OUTPUT"],
        'EXPRESSION': options["query"],
        'OUTPUT': 'memory:temp3'})

    temp4 = processing.run("native:simplifygeometries", {
        'INPUT': temp3["OUTPUT"],
        'METHOD': 0,
        'TOLERANCE': options["simplification"],
        'OUTPUT': 'memory:temp4'
    })

    temp5 = processing.run("native:buffer", {
        'INPUT': temp4["OUTPUT"],
        'DISTANCE': options["simplification"],
        'SEGMENTS': 1,
        'DISSOLVE': True,
        'OUTPUT': 'memory:temp5'
    })

    temp6 = processing.run("native:deleteholes", {
        'INPUT': temp5["OUTPUT"],
        'MIN_AREA': options["min_area"],
        'OUTPUT': 'memory:temp6'})

    temp7 = processing.run("native:symmetricaldifference", {
        'INPUT': temp6["OUTPUT"],
        'OVERLAY': temp1,
        'OVERLAY_FIELDS_PREFIX': '',
        'OUTPUT': 'memory:temp7'
    })

    temp8 = processing.run("native:multiparttosingleparts", {
        'INPUT': temp7["OUTPUT"],
        'OUTPUT': 'memory:temp8'})

    temp11 = processing.run("qgis:fieldcalculator", {
        'INPUT': temp8["OUTPUT"],
        'FIELD_NAME': options["attributes"][1][0], 'FIELD_TYPE': options["attributes"][1][1], 'FIELD_LENGTH': 10, 'FIELD_PRECISION': 3, 'NEW_FIELD': True,
        'FORMULA': 'area($geometry)', 'OUTPUT': 'memory:temp11'})

    temp12 = processing.run("native:extractbyexpression", {
        'INPUT': temp11["OUTPUT"],
        'EXPRESSION': options["attributes"][1][0] + ' > ' + str(options["min_area"]),
        'OUTPUT': 'memory:temp12'})

    layer = temp12["OUTPUT"]
    areas = layer.getFeatures()
    for area in areas:
        geom = area.geometry()
        buffer1 = geom.buffer(options["simplification"]*2, 5)
        buffer2 = buffer1.buffer(options["simplification"]*-2, 5)
        simplified = buffer2.simplify(options["simplification"]*2)
        layer.dataProvider().changeGeometryValues({area.id(): simplified})

    return temp12["OUTPUT"]


def saveFileToDisk(layer, path):
    save_options = QgsVectorFileWriter.SaveVectorOptions()
    save_options.driverName = "GPKG"
    error = QgsVectorFileWriter.writeAsVectorFormatV2(
        layer, path, QgsCoordinateTransformContext(), save_options)
    if error[0] == QgsVectorFileWriter.NoError:
        print("writing successful!                                         ")
    else:
        print(error)


def buildsubareas(raw_buildings, mainarea, lod, lines, Counter, epsg_code, min_req_bui):

    def addAreaIds(layer, Counter, attribute):
        layer.startEditing()
        area_id_field = QgsField(attribute[0], attribute[1])
        layer.addAttribute(area_id_field)
        layer.updateFields()

        features = layer.getFeatures()
        for feature in features:
            layer.changeAttributeValue(feature.id(), feature.fields().indexOf("area_id"), Counter.number)
            Counter.increase()
        layer.commitChanges()

    def toOutput(origin, output, attributes):

        new_features = origin.getFeatures()
        fields = output.fields()
        for feat in new_features:
            feature = QgsFeature()
            feature.setFields(fields)
            for attribute in attributes:
                feature[attribute[0]] = feat[attribute[0]]

            feature.setGeometry(feat.geometry())
            output.dataProvider().addFeature(feature)

    # simplification = parameter for simplification and buffer size of areas
    # min_area = in square meters, areas below this value will be dropped
    options = {
        "simplification": 5,
        "min_area": 2000,
        "epsg_code": epsg_code,
        "query": lod.query,
        "attributes": [["area_id", QVariant.Int], ["area_size", QVariant.Int]]
    }

    main_area_file = QgsVectorLayer(mainarea.file, "ogr")
    lines_raw = QgsVectorLayer(lines, "ogr")
    out_layer = QgsVectorLayer('MultiPolygon?crs=epsg:' + str(options["epsg_code"]), 'temp', "memory")

    out_layer.startEditing()
    for attribute in options["attributes"]:
        out_layer.addAttribute(QgsField(attribute[0], attribute[1]))
    out_layer.updateFields()
    out_layer.commitChanges()

    print("Make Building Index")
    buildings_layer = QgsVectorLayer(raw_buildings, "ogr")
    building_index = QgsSpatialIndex(buildings_layer.getFeatures())

    count = 1
    out_layer.startEditing()
    features = main_area_file.getFeatures()
    amount_features = str(main_area_file.featureCount())
    print("Make Areas")
    for feature in features:
        print('Processing Area ' + str(count) + ' of ' + amount_features + ' \r', end="")

        temp = createsubareas(feature, lines_raw, options)

        # check if enough buildings in area
        filter_subareas(building_index, buildings_layer, temp, min_req_bui)

        addAreaIds(temp, Counter, options["attributes"][0])
        # copy features to output
        toOutput(temp, out_layer, options["attributes"])

        count += 1

    out_layer.commitChanges()
    saveFileToDisk(out_layer, lod.file)
