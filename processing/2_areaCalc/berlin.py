
import sys
import os
from datetime import datetime
sys.path.append(os.path.abspath('../'))

from config import berlin as b
from util.counter import Counter
from util.functions import createMasterArea, buildsubareas

#################################################################################################################

startTime = datetime.now()
Counter = Counter(1)

#################################################################################################################

# delete old files
print("clean up working directory")
for lod in b.lods:
    if lod.query is not None:
        if os.path.isfile(lod.file):
            os.remove(lod.file)
            print("delete " + lod.file)
        if os.path.isfile(lod.file + "-shm"):
            os.remove(lod.file + "-shm")
            print("delete " + lod.file + "-shm")
        if os.path.isfile(lod.file + "-wal"):
            os.remove(lod.file + "-wal")
            print("delete " + lod.file + "-wal")


for index, lod in enumerate(b.lods):
    if os.path.isfile(lod.file):
        print("LOD" + str(lod.level) + " area found at : " + lod.file)
        continue

    print("LOD" + str(lod.level) + " area not found at: " + lod.file + ". Generate...")
    if index == 0:
        createMasterArea(b.source, lod.file, Counter)
    else:
        buildsubareas(b.buildings_height, b.lods[index - 1], lod, b.lines, Counter, b.epsg, b.minimum_required_buildings)
        print("LOD" + str(lod.level) + " area generated: " + lod.file)

print("Area creation done...")
print("overall time: " + str(datetime.now() - startTime))
print("---------------------")

