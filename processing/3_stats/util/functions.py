

import sys
import os
import re
import numpy as np
import statistics

import time

from datetime import datetime

import sys
try:
    from osgeo import ogr, osr, gdal, gdalconst
except:
    sys.exit('ERROR: cannot find GDAL/OGR modules')

from qgis.core import (
     QgsApplication,
     QgsVectorLayer,
     QgsCoordinateReferenceSystem,
     QgsVectorFileWriter,
     QgsCoordinateTransformContext,
     QgsSpatialIndex,
     QgsField,
     QgsFeature,
     QgsFeatureRequest,
     QgsExpression,
     QgsRasterLayer
)
from qgis.analysis import (
    QgsInterpolator,
    QgsIDWInterpolator,
    QgsGridFileWriter
)


from qgis.analysis import QgsNativeAlgorithms

QgsApplication.setPrefixPath('/usr', True)
qgs = QgsApplication([], False)
qgs.initQgis()
sys.path.append('/usr/share/qgis/python/plugins')

import processing
from processing.core.Processing import Processing
Processing.initialize()
QgsApplication.processingRegistry().addProvider(QgsNativeAlgorithms())
from PyQt5.QtCore import QVariant
from processing.tools import dataobjects

#################################################################################################################

def addAttributes(buildings_layer, buildings_height_layer, stat_fields_building, lod_layer, stat_fields_area, level):

    buildings_height_layer.startEditing()
    for attribute in stat_fields_building:
        buildings_height_layer.addAttribute(QgsField(attribute[0] + "_LOD_" + str(level), attribute[1]))
    buildings_height_layer.updateFields()
    buildings_height_layer.commitChanges()

    buildings_layer.startEditing()
    for attribute in stat_fields_building:
        buildings_layer.addAttribute(QgsField(attribute[0] + "_LOD_" + str(level), attribute[1]))
    buildings_layer.updateFields()
    buildings_layer.commitChanges()

    lod_layer.startEditing()
    for attribute in stat_fields_area:
        lod_layer.addAttribute(QgsField(attribute, stat_fields_area[attribute][0]))
    lod_layer.updateFields()
    lod_layer.commitChanges()
    for attribute in stat_fields_area:
        stat_fields_area[attribute].append(lod_layer.dataProvider().fieldNameIndex(attribute))


def buildingsInArea(buildings):

    stats = {
        "amount_ht_bdg": 0,
        "mean_ht_bdg": 0,
        "diff_ht_bdg": 0,
        "highest_bdg": 0,
        "lowest_bdg": 0,
        "modal_ht_bdg": 0,
        "median_ht_bdg": 0
    }

    # extract height from building object to make statstics
    heights = []
    for building in buildings:
        heights.append(building["extHeight"])

    amount_buildings = len(buildings)
    amount_heights_buildings = len(heights)
    stats["amount_ht_bdg"] = int(amount_buildings)

    if amount_buildings > 1 and amount_heights_buildings > 1:
        height_values_np = np.array(heights)
        height_values_np = height_values_np.astype('float64')
        height_values_np.sort()
        stats["highest_bdg"] = int(height_values_np[len(height_values_np) - 1])
        stats["lowest_bdg"] = int(height_values_np[0])
        stats["diff_ht_bdg"] = round(float(height_values_np[len(height_values_np) - 1] - height_values_np[0]), 2)
        stats["mean_ht_bdg"] = round(float(statistics.mean(heights)), 1)
        stats["median_ht_bdg"] = round(float(statistics.median(heights)), 2)

        if amount_buildings > 2 and amount_heights_buildings > 2:
            #stats["median_ht_bdg"] = round(float(statistics.median(heights)), 2)
            try:
                stats["modal_ht_bdg"] = int(statistics.mode(heights))
            except:
                print("error modal calc")

    return stats


def areaLayerStats(layer, areas_buildings, areas_building_height, stat_fields_area):

    for area in areas_building_height:
        buildings = areas_building_height[area]
        stats = buildingsInArea(buildings)

        layer.changeAttributeValue(area.id(),  area.fields().indexOf("amount_ht_bdg"), stats["amount_ht_bdg"])
        layer.changeAttributeValue(area.id(),  area.fields().indexOf("mean_ht_bdg"), stats["mean_ht_bdg"])
        layer.changeAttributeValue(area.id(), area.fields().indexOf("diff_ht_bdg"), stats["diff_ht_bdg"])
        layer.changeAttributeValue(area.id(), area.fields().indexOf("highest_bdg"), stats["highest_bdg"])
        layer.changeAttributeValue(area.id(), area.fields().indexOf("lowest_bdg"), stats["lowest_bdg"])
        layer.changeAttributeValue(area.id(), area.fields().indexOf("modal_ht_bdg"), stats["modal_ht_bdg"])
        layer.changeAttributeValue(area.id(), area.fields().indexOf("median_ht_bdg"), stats["median_ht_bdg"])

    for area in areas_buildings:
        buildings = areas_buildings[area]
        layer.changeAttributeValue(area.id(), area.fields().indexOf("amount_bdg"), len(buildings))


def buildingLayerStats(layer, areas, stat_fields_building, lod):

    for area in areas:
        buildings = areas[area]
        field_name = str(stat_fields_building[0][0]) + "_LOD_" + str(lod.level)
        field_name_index = layer.dataProvider().fieldNameIndex(field_name)
        area_id = area["area_id"]
        for building in buildings:
            layer.changeAttributeValue(building.id(), field_name_index, area_id)


def getBuildingsForArea(lod_layer, build_idx, build_lay):

    tempAreas = {}
    lod_features = lod_layer.getFeatures()

    # get all building ids for area in spatial tree
    for feature in lod_features:
        building_intersection = build_idx.intersects(feature.geometry().boundingBox())
        tempAreas[feature] = building_intersection

    # replace building ids from tree with building objects
    for area in tempAreas:
        buildings = []
        for building_id in tempAreas[area]:
            buildings.append(build_lay.getFeature(building_id))
        tempAreas[area] = buildings

    # check if buildings from spatial tree are also in the area and not only in bbox of the tree, if yes append to new array
    areas = {}
    for area in tempAreas:
        # copy area to new object
        areas[area] = []
        buildings = tempAreas[area]
        for building_id_index, building in enumerate(buildings):
            if area.geometry().intersects(building.geometry()):
                areas[area].append(building)

    return areas


def basicStats(buildings_layer, buildings_index, buildings_height_layer, buildings_height_index, lod, city):

    lod_layer = QgsVectorLayer(lod.file, "ogr")
    stat_fields_area = city.stat_fields_area
    stat_fields_building = city.stat_fields_building

    addAttributes(buildings_layer, buildings_height_layer, stat_fields_building, lod_layer, stat_fields_area, lod.level)

    print("collect buildings for LOD")
    areas_buildings = getBuildingsForArea(lod_layer, buildings_index, buildings_layer)
    areas_building_height = getBuildingsForArea(lod_layer, buildings_height_index, buildings_height_layer)

    print(" edit stats buildings")
    buildings_height_layer.startEditing()
    buildingLayerStats(buildings_height_layer, areas_building_height, stat_fields_building, lod)
    buildings_height_layer.commitChanges()

    buildings_layer.startEditing()
    buildingLayerStats(buildings_layer, areas_buildings, stat_fields_building, lod)
    buildings_layer.commitChanges()

    print(" edit stats area")
    lod_layer.startEditing()
    areaLayerStats(lod_layer, areas_buildings, areas_building_height, stat_fields_area)
    lod_layer.commitChanges()


def prepareBuildingsForArea(city, lod, area, buildings_height_layer):

    temp = QgsVectorLayer('MultiPolygon?crs=epsg:' + str(city.epsg), '', "memory")
    query = '"' + str(city.stat_fields_building[0][0]) + '_LOD_' + str(lod.level) + \
            '" LIKE \'' + str(area[city.stat_fields_building[0][0]]) + '\''
    features = buildings_height_layer.getFeatures(QgsFeatureRequest().setFilterExpression(query))
    temp.dataProvider().addAttributes(buildings_height_layer.dataProvider().fields().toList())
    temp.updateFields()
    temp.dataProvider().addFeatures(features)
    temp.updateExtents()
    return temp


def writeGeoJsonFile(save_options, layer, path, driverName):
    save_options.driverName = driverName
    save_options.layerOptions = ["COORDINATE_PRECISION=0"]
    QgsVectorFileWriter.writeAsVectorFormatV2(layer, path, QgsCoordinateTransformContext(), save_options)


def deleteFolderWithContent(path):
    if os.path.isdir(path):
        files = os.listdir(path)
        for file in files:
            os.remove(path + file)
        os.rmdir(path)


def makeMeanForArea(city, area, extent, temp_path):

    extent_coords = str(extent.extent().xMinimum()) + "," + str(extent.extent().xMaximum()) + "," + str(extent.extent().yMinimum()) + "," + str(extent.extent().yMaximum())
    processing.run("native:createconstantrasterlayer", {
        'EXTENT': extent_coords, 'TARGET_CRS': QgsCoordinateReferenceSystem('EPSG:' + str(city.epsg)),
        'PIXEL_SIZE': 5, 'NUMBER': area["mean_ht_bdg"], 'OUTPUT_TYPE': 5,
        'OUTPUT': temp_path
    })


def makeMedianForArea(city, area, extent, temp_path):

    extent_coords = str(extent.extent().xMinimum()) + "," + str(extent.extent().xMaximum()) + "," + str(extent.extent().yMinimum()) + "," + str(extent.extent().yMaximum())
    processing.run("native:createconstantrasterlayer", {
        'EXTENT': extent_coords, 'TARGET_CRS': QgsCoordinateReferenceSystem('EPSG:' + str(city.epsg)),
        'PIXEL_SIZE': 5, 'NUMBER': area["median_ht_bdg"], 'OUTPUT_TYPE': 5,
        'OUTPUT': temp_path
    })


def makeIDdwForArea(city, area, lod, buildings_height_layer, temp_path, extent, resolution):

    idw_points = city.idw_path + 'points_' + str(area["area_id"]) + '.geojson'

    # returns only the building for the specific area
    temp1 = prepareBuildingsForArea(city, lod, area, buildings_height_layer)

    # write the building height data to points and to a file to read this file later in gdal
    temp2 = processing.run("native:centroids", {'INPUT': temp1, 'ALL_PARTS': False, 'OUTPUT': 'memory:temp2'})
    save_options = QgsVectorFileWriter.SaveVectorOptions()
    driverName = 'geojson'
    save_options.attributes = [temp2["OUTPUT"].fields().indexOf("extHeight")]
    writeGeoJsonFile(save_options, temp2["OUTPUT"], idw_points, driverName)

    # calculation for the size of pixel and size of the calculated area
    xExtent = int((int(extent.extent().xMaximum()) - int(extent.extent().xMinimum())) / resolution)
    yExtent = int((int(extent.extent().yMaximum()) - int(extent.extent().yMinimum()))/resolution)
    maxAus = max([xExtent, yExtent])

    # Make the IDW Grid
    idw = gdal.Grid(
        temp_path,
        idw_points,
        width=xExtent,
        height=yExtent,
        outputBounds=[extent.extent().xMinimum(), extent.extent().yMinimum(), extent.extent().xMaximum(), extent.extent().yMaximum()],
        zfield='extHeight',
        algorithm='invdist:power=2.0:smoothing=0.0:radius=' + str(maxAus) + 'max_points=12:min_points=0:nodata=0.0',
        outputType=gdalconst.GDT_Int16,
    )
    idw = None

    os.remove(idw_points)

    # Clip the Grid to the extend of the area so we dont have overlapping areas
    # clipped = gdal.Warp(idw_file_clipped, idw_file, cutlineDSName=idw_extend, cropToCutline=True, dstNodata=0)
    # clipped = None
    #
    # delete the files which are not used anymore
    # os.remove(idw_extend)


def create_raster(city, area, lod, buildings_height_layer, resolution):

    # if area["area_id"] == 115:
    # check if conditions for creating IDW fulfilled
    # get the area in a single layer for extent of idw and later clip
    extent = QgsVectorLayer('MultiPolygon?crs=epsg:' + str(city.epsg), 'extent', "memory")
    extent.dataProvider().addFeatures([area])
    extent.updateExtents()
    temp_path = city.idw_path + 'idw_' + str(area["area_id"]) + '_temp.tif'

    if city.idw_options["max_idw_features"] > area["amount_ht_bdg"] > city.idw_options["min_idw_features"]:
        print("IDW")
        makeIDdwForArea(city, area, lod, buildings_height_layer, temp_path, extent, resolution)
    else:
        # print("mean")
        # makeMeanForArea(city, area, extent, temp_path)
        print("median")
        makeMedianForArea(city, area, extent, temp_path)



    idw_extend = city.idw_path + 'extent_' + str(area["area_id"]) + '.geojson'
    # write the extend to a file to read this file later in gdal
    save_options = QgsVectorFileWriter.SaveVectorOptions()
    driverName = 'geojson'
    writeGeoJsonFile(save_options, extent, idw_extend, driverName)
    # Clip the Grid to the extend of the area so we don't have overlapping areas
    clipped = gdal.Warp(city.idw_path + 'idw_' + str(area["area_id"]) + '.tif', temp_path,
                        cutlineDSName=idw_extend, cropToCutline=True, dstNodata=0)
    clipped = None
    os.remove(idw_extend)
    os.remove(temp_path)


def makeInterpolation(city, lod, buildings_height_layer):

    lod_layer = QgsVectorLayer(lod.file, "ogr")
    areas = lod_layer.getFeatures()
    # Extent in meter for each pixel in x and y direction
    resolution = 5

    if os.path.exists(city.idw_path):
        deleteFolderWithContent(city.idw_path)
    os.mkdir(city.idw_path)

    count = 0

    for area in areas:
        count = count+1
        # print(count)
        # if count == 519:
        print("   make calc for area: " + str(count) + " of " + str(lod_layer.featureCount()))
        create_raster(city, area, lod, buildings_height_layer, resolution)
        # sys.exit()

    file_names = os.listdir(city.idw_path)
    files = []
    for file_name in file_names:
        files.append(city.idw_path + file_name)

    print("-merge IDW for LOD: " + str(lod.level))
    processing.run("gdal:merge", {
        'INPUT': files,
        'PCT': False, 'SEPARATE': False, 'NODATA_INPUT': 0, 'NODATA_OUTPUT': 0, 'OPTIONS': '', 'EXTRA': '',
        'DATA_TYPE': 1, 'OUTPUT': city.idw_result + 'merge_LOD_' + str(lod.level) + '.tif'})

    deleteFolderWithContent(city.idw_path)

