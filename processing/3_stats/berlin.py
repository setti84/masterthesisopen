
import sys
import os
from datetime import datetime
import statistics
sys.path.append(os.path.abspath('../'))

from config import berlin as b
from util.functions import basicStats, makeInterpolation

from qgis.core import (
     QgsApplication,
     QgsVectorLayer,
     QgsCoordinateReferenceSystem,
     QgsVectorFileWriter,
     QgsCoordinateTransformContext,
     QgsSpatialIndex,
     QgsField,
     QgsFeature,
     QgsFeatureRequest
)

from qgis.analysis import QgsNativeAlgorithms

QgsApplication.setPrefixPath('/usr', True)
qgs = QgsApplication([], False)
qgs.initQgis()
sys.path.append('/usr/share/qgis/python/plugins')

import processing
from processing.core.Processing import Processing
Processing.initialize()
QgsApplication.processingRegistry().addProvider(QgsNativeAlgorithms())
from PyQt5.QtCore import QVariant
from processing.tools import dataobjects



#################################################################################################################

startTime = datetime.now()

#################################################################################################################

print("Make Building Index")
buildings_height_layer = QgsVectorLayer(b.buildings_height, "ogr")
buildings_layer = QgsVectorLayer(b.buildings, "ogr")
buildings_index = QgsSpatialIndex(buildings_layer.getFeatures())
buildings_height_index = QgsSpatialIndex(buildings_height_layer.getFeatures())

for index, lod in enumerate(b.lods):
    if os.path.isfile(lod.file):
        print("-make stats for LOD: " + str(lod.level))
        # if lod.level == 3:
        basicStats(buildings_layer, buildings_index, buildings_height_layer, buildings_height_index, lod, b)
        makeInterpolation(b, lod, buildings_height_layer)

    else:
        print("-cant find file")

print("-merge all IDW's")
file_names = os.listdir(b.idw_result)
files = []
for file_name in file_names:
    if 'tif' in file_name:
        files.append(b.idw_result + file_name)

files.sort()
processing.run("gdal:merge", {
    'INPUT': files,
    'PCT': False, 'SEPARATE': False, 'NODATA_INPUT': 0, 'NODATA_OUTPUT': 0, 'OPTIONS': '', 'EXTRA': '',
    'DATA_TYPE': 1, 'OUTPUT': b.idw_result + 'all_LOD.tif'})

print("Stats creation done...")
print("Overall time: " + str(datetime.now() - startTime))
print("---------------------")

