

class City:
    def __init__(self, name, origin_source, source, buildings, buildings_height, lines, idw_path, lods, area_id,
                 minimum_required_buildings, building_values, epsg, stat_fields_area, stat_fields_building,
                 min_area_building, idw_options, idw_result, all_reference_buildings, reference_buildings):
        self.name = name
        self.origin_source = origin_source
        self.source = source
        self.buildings = buildings
        self.buildings_height = buildings_height
        self.lines = lines
        self.idw_path = idw_path
        self.lods = lods
        self.area_id = area_id
        self.minimum_required_buildings = minimum_required_buildings
        self.building_values = building_values
        self.epsg = epsg
        self.stat_fields_area = stat_fields_area
        self.stat_fields_building = stat_fields_building
        self.min_area_building = min_area_building
        self.idw_options = idw_options
        self.idw_result = idw_result
        self.all_reference_buildings = all_reference_buildings
        self.reference_buildings = reference_buildings

    def __str__(self):
        return "Name: {0} \n " \
               "     Origin Source path: {1} \n " \
               "     Source path: {2} \n " \
               "     All Buildings path: {3} \n " \
               "     Buildings height path: {4} \n " \
               "     Lines path: {5} \n " \
                "     IDW path: {6} \n " \
               "     Lods area path: {7} \n " \
               "     Area ID: {8} \n " \
               "     Minimum required buildings: {9} \n " \
               "     Building values: {10} \n " \
               "     EPSG code: {11} \n " \
               "     Stat fields area: {12} \n " \
               "     Stat fields building: {13} \n " \
               "     Min building Area: {14} \n " \
               "     IDW options: {15} \n " \
               "     Idw result path: {16} \n " \
               "     all reference Buildings Path: {17} \n " \
               "     reference Buildings Path: {18} \n " \
            .format(self.name, self.origin_source, self.source, self.buildings, self.buildings_height, self.lines,
                    self.idw_path, self.lods, self.area_id, self.minimum_required_buildings,
                    self.building_values, self.epsg, self.stat_fields_area, self.stat_fields_building,
                    self.min_area_building, self.idw_options, self.idw_result, self.all_reference_buildings,
                    self.reference_buildings)
