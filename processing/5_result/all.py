
import sys
import os
#from datetime import datetime
#import statistics
sys.path.append(os.path.abspath('../'))

from config import berlin as b
from config import phoenix as p
from config import daressalaam as d
# from util.functions import select_reference, prepareLayer

from qgis.core import (
     QgsApplication,
     QgsVectorLayer,
     QgsRasterLayer
#     QgsCoordinateReferenceSystem,
#     QgsVectorFileWriter,
#     QgsCoordinateTransformContext,
#     QgsSpatialIndex,
#     QgsField,
#     QgsFeature,
#     QgsFeatureRequest
)

from qgis.analysis import QgsNativeAlgorithms

QgsApplication.setPrefixPath('/usr', True)
qgs = QgsApplication([], False)
qgs.initQgis()
sys.path.append('/usr/share/qgis/python/plugins')

import processing
from processing.core.Processing import Processing
Processing.initialize()
QgsApplication.processingRegistry().addProvider(QgsNativeAlgorithms())
# from PyQt5.QtCore import QVariant
# from processing.tools import dataobjects
#####################################################################################################


######### berlin

print('calc Berlin')

buildings_reference1 = QgsVectorLayer(b.reference_buildings, "ogr")
merged_LODs1 = QgsRasterLayer(b.idw_result + 'all_LOD.tif', "ogr")

processing.run("native:zonalstatistics", {
     'INPUT_RASTER': merged_LODs1,
     'RASTER_BAND': 1,
     'INPUT_VECTOR': buildings_reference1,
     'COLUMN_PREFIX': '_',
     'STATISTICS': [0, 2, 3, 5, 6, 7, 8, 9, 10, 11]
})

######### phoenix

print('calc Phoenix')

buildings_reference2 = QgsVectorLayer(p.reference_buildings, "ogr")
merged_LODs2 = QgsRasterLayer(p.idw_result + 'all_LOD.tif', "ogr")

processing.run("native:zonalstatistics", {
     'INPUT_RASTER': merged_LODs2,
     'RASTER_BAND': 1,
     'INPUT_VECTOR': buildings_reference2,
     'COLUMN_PREFIX': '_',
     'STATISTICS': [0, 2, 3, 5, 6, 7, 8, 9, 10, 11]
})

######### daressalaam

# print('calc Dar Es Salaam')
#
# buildings_reference3 = QgsVectorLayer(d.reference_buildings, "ogr")
# merged_LODs3 = QgsRasterLayer(d.idw_result + 'all_LOD.tif', "ogr")
#
# processing.run("native:zonalstatistics", {
#      'INPUT_RASTER': merged_LODs3,
#      'RASTER_BAND': 1,
#      'INPUT_VECTOR': buildings_reference3,
#      'COLUMN_PREFIX': '_',
#      'STATISTICS': [0, 2, 3, 5, 6, 7, 8, 9, 10, 11]
# })