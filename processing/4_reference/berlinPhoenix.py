
import sys
import os
#from datetime import datetime
#import statistics
sys.path.append(os.path.abspath('../'))

from config import berlin as b
from config import phoenix as p
from config import daressalaam as d
from util.functions import select_reference, prepareLayer, writeGPKGFile

from qgis.core import (
     QgsApplication,
     QgsVectorLayer
)

from qgis.analysis import QgsNativeAlgorithms

QgsApplication.setPrefixPath('/usr', True)
qgs = QgsApplication([], False)
qgs.initQgis()
sys.path.append('/usr/share/qgis/python/plugins')

import processing
from processing.core.Processing import Processing
Processing.initialize()
QgsApplication.processingRegistry().addProvider(QgsNativeAlgorithms())
# from PyQt5.QtCore import QVariant
# from processing.tools import dataobjects
#####################################################################################################

# write new random 100 buldings

#####################  Berlin ###############################

building_ref = select_reference(b.all_reference_buildings, b.reference_buildings)

#####################  Phoenix ###############################

# building_ref = select_reference(p.all_reference_buildings, p.reference_buildings)

#####################  Dar Es Salaam ###############################

# There are only 100 buildings in the reference data so the layer is just a duplicate because of name convention

##################################################################################################

# old
# temp_path = './../../source-data/berlin/referenceData/berlin_100samples.GPKG'
# if not os.path.isfile(temp_path):
#     print("create prepared Layer")
#     temp = prepareLayer(buildings_reference_all)
#     writeGPKGFile(temp, temp_path)