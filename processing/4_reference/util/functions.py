import sys
from statistics import median

from qgis.core import (
     QgsApplication,
     QgsVectorLayer,
     QgsCoordinateReferenceSystem,
     QgsVectorFileWriter,
     QgsCoordinateTransformContext,
     QgsSpatialIndex,
     QgsField,
     QgsFeature,
     QgsFeatureRequest,
     QgsExpression,
     QgsRasterLayer
)

from qgis.analysis import QgsNativeAlgorithms

QgsApplication.setPrefixPath('/usr', True)
qgs = QgsApplication([], False)
qgs.initQgis()
sys.path.append('/usr/share/qgis/python/plugins')
import processing
from processing.core.Processing import Processing
Processing.initialize()
QgsApplication.processingRegistry().addProvider(QgsNativeAlgorithms())

# from PyQt5.QtCore import QVariant
# from processing.tools import dataobjects


##################################################################################################################

def calculate_mean_height(layer):
    features = layer.getFeatures()

    parent_ids = {}
    for feature in features:
        if isinstance(feature["parentid"], str) and type(feature["height"]) is float:
            if feature["parentid"] not in parent_ids:
                parent_ids[feature["parentid"]] = []
            parent_ids[feature["parentid"]].append(feature["height"])
    mean_heights = {}
    for parentID, heights in parent_ids.items():
        mean_heights[parentID] = round(median(heights), 1)

    layer.startEditing()
    for feature in layer.getFeatures():
        if type(feature["parentid"]) is str:
            mean_height = mean_heights.get(feature["parentid"])
            if type(mean_height) is float:
                layer.changeAttributeValue(feature.id(), feature.fields().indexOf("meanHeight"), mean_height)
        else:
            # print(feature["height"])
            layer.changeAttributeValue(feature.id(), feature.fields().indexOf("meanHeight"), feature["height"])
    layer.commitChanges()
    return layer


def select_reference(layer, target_path):
    # Vorausetzungen für referenzdaten: keine Parent ID, min und max fläche, min und max height
    print("select reference")
    temp1 = processing.run("native:extractbyexpression", {
         'INPUT': layer,
         'EXPRESSION': '\"height\" > 2 ',
         'OUTPUT': 'memory:temp1'})
    temp2 = processing.run("native:randomextract", {
         'INPUT': temp1["OUTPUT"],
         'METHOD': 0, 'NUMBER': 100,
         'OUTPUT': target_path})
    return temp2


def prepareLayer(layer):

    temp1 = processing.run("native:fixgeometries", {
        'INPUT': layer,
        'OUTPUT': 'memory:temp1'})

    print("calculate area field")
    temp2 = processing.run("qgis:fieldcalculator", {
        'INPUT': temp1["OUTPUT"],
        'FIELD_NAME': 'area', 'FIELD_TYPE': 1,  'NEW_FIELD': True, 'FORMULA': '$area',
        'OUTPUT': 'memory:temp2'})

    temp3 = processing.run("native:addfieldtoattributestable", {
        'INPUT': temp2["OUTPUT"],
        'FIELD_NAME': 'meanHeight', 'FIELD_TYPE': 1, 'FIELD_LENGTH': 10, 'FIELD_PRECISION': 1,
        'OUTPUT': 'memory:temp3'})

    print("calculate mean height for buildings")
    #temp_layer = calculate_mean_height(temp3["OUTPUT"])
    return temp3["OUTPUT"]

# writeGPKGFile(temp_layer, './../../source-data/berlin/referenceData/berlinTest4.GPKG')
def writeGPKGFile(layer, path):
    save_options = QgsVectorFileWriter.SaveVectorOptions()
    save_options.driverName = "GPKG"
    error = QgsVectorFileWriter.writeAsVectorFormatV2(
        layer, path, QgsCoordinateTransformContext(), save_options)
    if error[0] == QgsVectorFileWriter.NoError:
        print("writing successful!                                         ")
    else:
        print(error)
