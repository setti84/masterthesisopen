class Lod:
    def __init__(self, level, file, query):
        self.level = level
        self.file = file
        self.query = query

    def info(self):
        print('level: ' + str(self.level) + '\n File: ' + str(self.file) + '\n Query: ' + str(self.query))
